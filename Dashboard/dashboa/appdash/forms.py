from django.forms import ModelForm, TextInput
from appdash.models import DimEstudiante
#from .models import DimEstudiante

class EstudianteForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # for form in self.visible_fields():
            # form.field.widget.attrs['class'] = 'form-control'
            # form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['estu_consecutivo'].widget.attrs['autofocus'] = True

    class Meta:
        model = DimEstudiante
        fields = '__all__'
        widgets = {
            'id_estudiante': TextInput(
                attrs={
                    'placeholder': 'ingrese un id',
                }
            ),
            'estu_consecutivo': TextInput(
                attrs={
                    'placeholder': 'ingrese consecutivo',
                }
            ),
            'estu_grupo': TextInput(
                attrs={
                    'placeholder': 'ingrese grupo',
                }
            ),
            'estu_n': TextInput(
                attrs={
                    'placeholder': 'ingrese n',
                }
            ),
            'estu_estrato': TextInput(
                attrs={
                    'placeholder': 'ingrese estrato',
                }
            ),
            'estu_grado': TextInput(
                attrs={
                    'placeholder': 'ingrese grado',
                }
            ),
            'estu_sexo': TextInput(
                attrs={
                    'placeholder': 'ingrese sexo',
                }
            ),
            'estu_dissenso': TextInput(
                attrs={
                    'placeholder': 'ingrese dissenso',
                }
            )
        }



    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                form.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

