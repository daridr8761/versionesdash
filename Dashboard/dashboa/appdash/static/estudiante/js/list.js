$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id_estudiante"},
            {"data": "estu_consecutivo"},
            {"data": "estu_grupo"},
            {"data": "estu_n"},
            {"data": "estu_estrato"},
            {"data": "estu_grado"},
            {"data": "estu_sexo"},
            {"data": "estu_dissenso"},
        ],
        columnDefs: [
            {
                targets: [],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                }
            },
        ],
        initComplete: function (settings, json) {
        }
    });
});