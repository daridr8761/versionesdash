$(function () {
    var $NumEstuPorMunicipio = $("#NumEstuPorMunicipio");

    $.ajax({
        url: $NumEstuPorMunicipio.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipio[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica1 = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                    enabled: true,
                                    mode: "xy",
                                    speed: 10,
                                    threshold: 10
                                },
                        zoom: {
                                    enabled: true,
                                    drag: false,
                                    mode: "xy",
                                    speed: 0.01, //acercar con rueda mouse
                                    // sensitivity: 0.1,
                                    limits: {
                                                max: 10,
                                                min: 0.5
                                            }
                               }
                 }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
       }
    });



})


$(function () {
    var $NumEstuPorMunicipiodos = $("#NumEstuPorMunicipiodos");

    $.ajax({
        url: $NumEstuPorMunicipiodos.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiodos[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica2 = new Chart(ctx, {
          type: 'line',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})

$(function () {
    var $NumEstuPorMunicipiotres = $("#NumEstuPorMunicipiotres");

    $.ajax({
        url: $NumEstuPorMunicipiotres.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiotres[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica3 = new Chart(ctx, {
          type: 'radar',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})


$(function () {
    var $NumEstuPorMunicipiocuatro = $("#NumEstuPorMunicipiocuatro");

    $.ajax({
        url: $NumEstuPorMunicipiocuatro.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiocuatro[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica4 = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})

$(function () {
    var $NumEstuPorMunicipio_B = $("#NumEstuPorMunicipio_B");

    $.ajax({
        url: $NumEstuPorMunicipio_B.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipio_B[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica1_B = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                    enabled: true,
                                    mode: "xy",
                                    speed: 10,
                                    threshold: 10
                                },
                        zoom: {
                                    enabled: true,
                                    drag: false,
                                    mode: "xy",
                                    speed: 0.01, //acercar con rueda mouse
                                    // sensitivity: 0.1,
                                    limits: {
                                                max: 10,
                                                min: 0.5
                                            }
                               }
                 }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
       }
    });

})



$(function () {
    var $NumEstuPorMunicipiodos_L = $("#NumEstuPorMunicipiodos_L");

    $.ajax({
        url: $NumEstuPorMunicipiodos_L.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiodos_L[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica2_L = new Chart(ctx, {
          type: 'line',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})


$(function () {
    var $NumEstuPorMunicipiotres_R = $("#NumEstuPorMunicipiotres_R");

    $.ajax({
        url: $NumEstuPorMunicipiotres_R.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiotres_R[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica3_R = new Chart(ctx, {
          type: 'radar',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})

$(function () {
    var $NumEstuPorMunicipiocuatro_D = $("#NumEstuPorMunicipiocuatro_D");

    $.ajax({
        url: $NumEstuPorMunicipiocuatro_D.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiocuatro_D[0].getContext("2d");


        var NumEstu = {
              label: 'Numero de Estudiantes',
              backgroundColor: 'blue',
              data:data.yNumEstu
        };

        window.grafica4_D = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: data.xAnios,
            datasets: [NumEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Numero de Estudiantes por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})

//Funcion oculta y desoculta grafica o tabla segun seleccione el usuario
/*function generar(){
                var tabla = document.getElementById("tab")
                var grafica = document.getElementById("graf")
                var se = document.getElementById("boton").value

                if (se == "Grafica")
                    {
                        grafica.style.display='block'
                        tabla.style.display='none'
                    }
                    else
                    {
                        if (se == "Tabla")
                        {
                            grafica.style.display='none'
                            tabla.style.display='block'
                        }
                    }
}*/