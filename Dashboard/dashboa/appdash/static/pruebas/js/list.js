$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id_prueba"},
            {"data": "prue_estu_consecutivo"},
            {"data": "prue_sn_lenguaje"},
            {"data": "prue_sn_matematicas"},
            {"data": "prue_sn_ciencias_naturales"},
            {"data": "prue_sn_competencias"},
            {"data": "des_lenguaje"},
            {"data": "des_matematicas"},
            {"data": "des_ciencias_naturales"},
            {"data": "des_competencias"},
        ],
        columnDefs: [
            {
                targets: [],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                }
            },
        ],
        initComplete: function (settings, json) {
        }
    });
});