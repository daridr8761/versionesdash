$(document).ready(function (){

        $("#form").submit(function (e){
            e.preventDefault();
            var $graficGest = $("#graficaNumEstu");
            var estu = document.getElementById("estudiante").options[document.getElementById("estudiante").selectedIndex].text;
            var muni = document.getElementById("municipio").options[document.getElementById("municipio").selectedIndex].text;
            var inst = document.getElementById("inst").options[document.getElementById("inst").selectedIndex].text;
            var ano = document.getElementById("anios").options[document.getElementById("anios").selectedIndex].text;
            var cat = document.getElementById("cat").options[document.getElementById("cat").selectedIndex].text;
            var grafi = document.getElementById("graf").value;
            let ban=0;

             $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize(),

                 success: function (data){
                    var ctx = $graficGest[0].getContext("2d");
                    function getRandomColor() {    //cambiar color cada nueva grafica
                                var letters = "0123456789ABCDEF".split("");
                                var color = "#";
                                for (var i = 0; i < 6; i++ ) {
                                  color += letters[Math.floor(Math.random() * 16)];
                                }
                                return color;
                            }

                            var estudiante = {
                              label: 'N- Estudiante',
                                  backgroundColor: [
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                  ],
                                  data:data.data,
                                  borderColor: 'red',
                                  lineTension: 0,
                                  fill: false,
                            };

                            //Numero de estudiantes Clasificados por Genero
                            var hombres = {
                                  label: 'Hombres',
                                  backgroundColor: 'green',
                                  data:data.hombres,
                                  borderColor: 'green',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'A'
                                   };

                            var mujeres = {
                                  label: 'Mujeres',
                                  backgroundColor: 'Blue',
                                  data:data.mujeres,
                                  borderColor: 'Blue',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'A'
                                   };

                            var no_espe = {
                              label: 'No Especifica',
                                  backgroundColor: 'red',
                                  data:data.no_espe,
                                  borderColor: 'red',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'A'
                            };

                            // Numero de estudiantes por tipo de Establecimiento
                            var oficial_r = {
                                  label: 'Oficial Rural',
                                  backgroundColor: 'green',
                                  data:data.oficial_r,
                                  borderColor: 'green',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'B'
                            };

                            var no_oficial = {
                              label: 'No Oficial',
                                  backgroundColor: 'Blue',
                                  data:data.no_oficial,
                                  borderColor: 'Blue',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'B'
                            };

                            var oficial_u = {
                              label: 'Oficial Urbano',
                                  backgroundColor: 'red',
                                  data:data.oficial_u,
                                  borderColor: 'red',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'B'
                            };

                            //Numero de estudiantes Clasificados por Zona

                            var rural = {
                                  label: 'Zona Rural',
                                  backgroundColor: 'green',
                                  data:data.rural,
                                  borderColor: 'green',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'C'
                            };

                            var urbano = {
                                  label: 'Zona Urbana',
                                  backgroundColor: 'red',
                                  data:data.urbano,
                                  borderColor: 'red',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'C'
                            };

                            //Numero de estudiantes con o sin Discapacidad Cognitiva
                            var con_discapacidad = {
                                  label: 'Estudiantes con Discapacidad',
                                  backgroundColor: 'green',
                                  data:data.con_discapacidad,
                                  borderColor: 'green',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'D'
                                   };

                            var sin_discapacidad = {
                                  label: 'Estudiantes sin Discapacidad',
                                  backgroundColor: 'Blue',
                                  data:data.sin_discapacidad,
                                  borderColor: 'Blue',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'D'
                                   };

                            //Numero de estudiantes por Desempeño Global
                            var insuficiente = {
                                  label: 'Desempeño Insuficiente',
                                  backgroundColor: 'red',
                                  data:data.insuficiente,
                                  borderColor: 'red',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'E'
                                   };

                            var minimo = {
                                  label: 'Desempeño Minimo',
                                  backgroundColor: 'orange',
                                  data:data.minimo,
                                  borderColor: 'orange',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'E'
                                   };

                            var satisfactorio = {
                                  label: 'Desempeño Satisfactorio',
                                  backgroundColor: 'green',
                                  data:data.satisfactorio,
                                  borderColor: 'green',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'E'
                                   };

                            var avanzado = {
                                  label: 'Desempeño Avanzado',
                                  backgroundColor: 'Blue',
                                  data:data.avanzado,
                                  borderColor: 'Blue',
                                  lineTension: 0,
                                  fill: false,
                                  yAxisID: 'E'
                                   };

                            if (window.grafica) {  // limpiar grafica anterior
                                window.grafica.clear();
                                window.grafica.destroy();
                            }

                            if(grafi=="tabla"){
                                grafi="bar";
                                ban=1;
                            }

                            let dat={

                            }

                            if (cat=='Genero'){
                                dat={
                                    labels: data.labels,
                                    datasets: [hombres,mujeres,no_espe]
                                }
                            }
                            else
                            if(cat=='Tipo de Establecimiento'){
                                dat={
                                    labels: data.labels,
                                    datasets: [oficial_r,no_oficial,oficial_u]
                                }
                            }
                            else
                            if(cat=='Zona'){
                                dat={
                                    labels: data.labels,
                                    datasets: [rural,urbano]
                                }
                            }
                            else
                            if(cat=='Discapacidad Cognitiva'){
                                dat={
                                    labels: data.labels,
                                    datasets: [con_discapacidad,sin_discapacidad]
                                }
                            }
                            else
                            if(cat=='Nivel de Desempeño Global'){
                                dat={
                                    labels: data.labels,
                                    datasets: [insuficiente,minimo,satisfactorio,avanzado]
                                }
                            }



                             window.grafica = new Chart(ctx, {
                              type: grafi,
                              data: dat,
                              options: {
                                tooltips: {
                                    callbacks: {
                                        label: function(item, data) {
                                            console.log(data.labels, item);
                                            return grafi=="pie"||grafi=="polarArea"||grafi=="doughnut"?data.datasets[item.datasetIndex].label+ ": "+ data.labels[item.index]+ ": "+ data.datasets[item.datasetIndex].data[item.index]:data.datasets[item.datasetIndex].label+ ": "+ data.datasets[item.datasetIndex].data[item.index];
                                        }
                                    },
                                },
                                responsive: true,
                                legend: {
                                  position: 'top',
                                },
                                title: {
                                  display: true,
                                  text: 'ESTUDIANTE: '+estu +'     CATEGORIA: '+cat+'     MUNICIPIO: '+muni+'     INSTITUCION: '+inst+'     AÑO: '+ano
                                },
                                scales: {
                                     yAxes: [{
                                                  //id: (cat=='Genero') ? 'A' : 'B',
                                                  id: (cat=='Genero') ? 'A' : (cat=='Tipo de Establecimiento') ? 'B' : (cat=='Zona') ? 'C' : (cat=='Discapacidad Cognitiva') ? 'D' : 'E',
                                                  //id: (cat=='Discapacidad Cognitiva') ? 'D' : 'E',
                                                  position: 'left',
                                                  ticks: {
                                                            min: 0,

                                                            //callback: function(value) { return value + "%" }
                                                          },
                                                  scaleLabel: {
                                                      display: true,
                                                      labelString: 'Numero Estudiante'
                                                  }
                                     }]
                                },
                                pan: {
                                  enabled: true,
                                  mode: "x",
                                  speed: 10,
                                  threshold: 10
                                },
                                zoom: {
                                    enabled: true,
                                    drag: false,
                                    mode: "x",
                                    speed: 0.01,
                                    sensitivity: 0.1,
                                    limits: {
                                        max: 10,
                                        min: 0.5
                                    }
                                }
                              }
                            });
                     var graf=document.getElementById("graficaNumEstu");
                     var botones=document.getElementById("graf_R");
                     var tab=document.getElementById("tabla");

                     if(ban==1){

                                    graf.style.display='none';
                                    botones.style.display='none';
                                    tab.style.display='block';
                                    const tableContainer = document.getElementById('tabla');
                                    const xAxis = grafica.data.labels;
                                    const yAxis = grafica.data.datasets;

                                    const tableHeader = `<tr>${
                                        xAxis.reduce((memo, entry) => {
                                            memo += `<th>${entry}</th>`;
                                            return memo;
                                        }, '<th>METRICAS:</th>')
                                    }</tr>`;

                                    const tableBody = yAxis.reduce((memo, entry) => {
                                        const rows = entry.data.reduce((memo, entry) => {
                                            memo += `<td>${entry}</td>`
                                            return memo;
                                        }, '');

                                    memo += `<tr><td>${entry.label}</td>${rows}</tr>`; //permite separar los datos en celdas diferentes

                                     return memo;
                                    }, '');

                                    console.log(data);
                                    const table = ` <div class="container-fluid">
                                                        <div class="card shadow mb-6">
                                                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                                        <h6 class="m-0 font-weight-bold text-dark">METRICA >> ESTUDIANTE:${estu}&nbsp;&nbsp;&nbsp;CATEGORIA:${cat}&nbsp;&nbsp;&nbsp;<br> <br>MUNICIPIO: ${muni}&nbsp;&nbsp;&nbsp;INSTITUCION: ${inst}&nbsp;&nbsp;&nbsp;AÑO: ${ano}</h6>
                                                        </div>
                                                                <div class="card-body">
                                                                    <div class="table-responsive">
                                                                        <table class="table table-striped table-bordered" id="dataTable" width="100%" cellspacing="0">
                                                                            <thead>
                                                                                ${tableHeader}
                                                                            </thead>
                                                                            <tbody>
                                                                                ${tableBody}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <script>
                                                            $('#dataTable').DataTable({
                                                                        "pagingType": "full_numbers"
                                                            });
                                                        </script>
                                                    </div>`;


                                    tableContainer.innerHTML = table;
                                    ban=0;

                             }else{
                                    graf.style.display='block';
                                    botones.style.display='block';
                                    tab.style.display='none';
                             }


                 }

             })

        })

})
