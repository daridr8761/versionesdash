$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id_tiempo"},
            {"data": "prueba"},
            {"data": "anio"},
        ],
        columnDefs: [
            {
                targets: [],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                }
            },
        ],
        initComplete: function (settings, json) {
        }
    });
});