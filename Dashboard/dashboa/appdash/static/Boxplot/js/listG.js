$(document).ready(function (){

    $("#form").submit(function (e){
            e.preventDefault();


                    var $NumEstuPorMunicipiocinco = $("#NumEstuPorMunicipiocinco");
                    var $PunEstuParaMate = $("#PunEstuParaMate");
                    var $PunEstuParaCien = $("#PunEstuParaCien");
                    var $PunEstuParaComp = $("#PunEstuParaComp");
                    var ano = document.getElementById("anios").options[document.getElementById("anios").selectedIndex].text;


                    $.ajax({
                        url: $(this).attr('action'),
                        type: $(this).attr('method'),
                        data: $(this).serialize(),

                      success: function (data) {

                        //var ctx = $NumEstuPorMunicipiocinco[0].getContext("2d");

                        if(ano=="2014"){
                        graf_a.style.display='block';
                        graf_b.style.display='none';
                        graf_c.style.display='none';
                        graf_d.style.display='none';
                        var ctx = $NumEstuPorMunicipiocinco[0].getContext("2d");
                        }
                        else
                        if(ano=="2015"){
                        graf_a.style.display='none';
                        graf_b.style.display='block';
                        graf_c.style.display='none';
                        graf_d.style.display='none';
                        var ctx = $PunEstuParaMate[0].getContext("2d");
                        }
                        else
                        if(ano=="2016"){
                        graf_a.style.display='none';
                        graf_b.style.display='none';
                        graf_c.style.display='block';
                        graf_d.style.display='none';
                        var ctx = $PunEstuParaCien[0].getContext("2d");
                        }
                        else
                        if(ano=="2017"){
                        graf_a.style.display='none';
                        graf_b.style.display='none';
                        graf_c.style.display='none';
                        graf_d.style.display='block';
                        var ctx = $PunEstuParaComp[0].getContext("2d");
                        }



                        window.grafica5 = new Chart(ctx, {
                          type: 'boxplot',
                          data: {
                            labels: [ano],
                            datasets: [{

                                            label: 'LENGUAJE',
                                            backgroundColor: 'rgba(255, 26, 104, 0.2)',
                                            borderColor: 'rgba(255, 26, 104, 1)',
                                            borderWidth: 1,
                                            outlierColor: '#999999',
                                            padding: 0,
                                            itemRadius: 3,
                                                //data: [18, 12, 6, 9, 12, 3, 9],
                                                data: [
                                                        data.total_yPuntLengEstu
                                                        // Con la anterior linea graficamos con la data de la BD postgres y Con randomVales graficamos valore estaticos para formar el diagrama de cajas
                                                        /*randomValues(100, 0, 100), // min wisker o bgote minimo
                                                        randomValues(100, 0, 20), // Lower Quartile o cuartil inferior
                                                        randomValues(100, 20, 70), // median o media
                                                        randomValues(100, 60, 100), // Inter Quartil Range (IQR)
                                                        randomValues(40, 50, 100), // mean o media
                                                        randomValues(100, 60, 120), // Upper Quartil o cuartil superior
                                                        randomValues(100, 80, 100), // max wisker o bigote maximo*/
                                                      ]
                                     },{
                                            label: 'Matematicas',
                                            backgroundColor:  'rgba(0,0,255,0.2)',
                                            borderColor: 'rgba(0, 0, 255, 1)',
                                            borderWidth: 1,
                                            outlierColor: '#999999',
                                            padding: 10,
                                            itemRadius: 3,

                                                data: [
                                                        data.total_yPuntMatEstu
                                                        // Con la anterior linea graficamos con la data de la BD postgres y Con randomVales graficamos valore estaticos para formar el diagrama de cajas
                                                        /*randomValues(100, 0, 100), // min wisker o bgote minimo
                                                        randomValues(100, 0, 20), // Lower Quartile o cuartil inferior
                                                        randomValues(100, 20, 70), // median o media
                                                        randomValues(100, 60, 100), // Inter Quartil Range (IQR)
                                                        randomValues(40, 50, 100), // mean o media
                                                        randomValues(100, 60, 120), // Upper Quartil o cuartil superior
                                                        randomValues(100, 80, 100), // max wisker o bigote maximo*/
                                                     ]
                                      },{
                                            label: 'Ciencias',
                                            backgroundColor:  'rgba(0,122,0,0.5)',
                                            borderColor: 'rgba(0, 122, 0, 1)',
                                            borderWidth: 1,
                                            outlierColor: '#999999',
                                            padding: 10,
                                            itemRadius: 3,

                                                data: [
                                                        data.total_yPuntCiencEstu
                                                        // Con la anterior linea graficamos con la data de la BD postgres y Con randomVales graficamos valore estaticos para formar el diagrama de cajas
                                                        /*randomValues(100, 0, 100), // min wisker o bgote minimo
                                                        randomValues(100, 0, 20), // Lower Quartile o cuartil inferior
                                                        randomValues(100, 20, 70), // median o media
                                                        randomValues(100, 60, 100), // Inter Quartil Range (IQR)
                                                        randomValues(40, 50, 100), // mean o media
                                                        randomValues(100, 60, 120), // Upper Quartil o cuartil superior
                                                        randomValues(100, 80, 100), // max wisker o bigote maximo*/
                                                     ]
                                      },{
                                            label: 'Competencias',
                                            backgroundColor:  'rgba(130,0,176,0.2)',
                                            borderColor: 'rgba(130, 0, 176, 1)',
                                            borderWidth: 1,
                                            outlierColor: '#999999',
                                            padding: 10,
                                            itemRadius: 3,

                                                data: [
                                                        data.total_yPuntCompeEstu
                                                        // Con la anterior linea graficamos con la data de la BD postgres y Con randomVales graficamos valore estaticos para formar el diagrama de cajas
                                                        /*randomValues(100, 0, 100), // min wisker o bgote minimo
                                                        randomValues(100, 0, 20), // Lower Quartile o cuartil inferior
                                                        randomValues(100, 20, 70), // median o media
                                                        randomValues(100, 60, 100), // Inter Quartil Range (IQR)
                                                        randomValues(40, 50, 100), // mean o media
                                                        randomValues(100, 60, 120), // Upper Quartil o cuartil superior
                                                        randomValues(100, 80, 100), // max wisker o bigote maximo*/
                                                     ]
                                      }]
                          },
                          options: {
                                                        scales: {
                                                          y: {
                                                            beginAtZero: false
                                                          }
                                                        }
                                    }
                        });

                        function randomValues(count, min, max)
                        {
                                const delta = max - min;
                                return Array.from({length: count}).map(() => Math.random() * delta + min);

                        }

                        //var cont = data.ynum;
                        //var nom = data.xmuni;
                      }
                    });
     })

})