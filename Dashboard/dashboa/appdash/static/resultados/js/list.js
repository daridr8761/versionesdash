$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id_estudiante"},
            {"data": "muni_id_ente"},
            {"data": "id_institucion"},
            {"data": "id_prueba"},
            {"data": "id_tiempo"},
            {"data": "id_hoja"},
            {"data": "numero_estu"},
            {"data": "puntaje_lenguaje"},
            {"data": "puntaje_matematicas"},
            {"data": "puntaje_ciencias"},
            {"data": "puntaje_competencias"},
            {"data": "numero_lenguaje"},
            {"data": "numero_matematicas"},
            {"data": "numero_ciencias"},
            {"data": "numero_competencias"},
            {"data": "anio"},
            {"data": "promedio"},
            {"data": "desempenio"},
        ],
        columnDefs: [
            {
                targets: [],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                }
            },
        ],
        initComplete: function (settings, json) {
        }
    });
});