
$(function () {
    var $NumEstuPorMunicipio = $("#NumEstuPorMunicipio");

    $.ajax({
        url: $NumEstuPorMunicipio.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipio[0].getContext("2d");

        function getRandomColor() {    //cambiar color cada nueva grafica
                                var letters = "0123456789ABCDEF".split("");
                                var color = "#";
                                for (var i = 0; i < 6; i++ ) {
                                  color += letters[Math.floor(Math.random() * 16)];
                                }
                                return color;
                            }

        var PuntLengEstu = {
              label: 'Puntaje Prueba de Lenguaje',
              //backgroundColor: 'blue',
              backgroundColor: [
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                    getRandomColor(),
                                  ],
              data:data.yPuntLengEstu
        };

        window.grafica1 = new Chart(ctx, {
          type: 'bar',
          data: {
            labels: data.xAnios,
            datasets: [PuntLengEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Puntaje Lenguaje por Año'
                        },
                        pan: {
                                    enabled: true,
                                    mode: "xy",
                                    speed: 10,
                                    threshold: 10
                                },
                        zoom: {
                                    enabled: true,
                                    drag: false,
                                    mode: "xy",
                                    speed: 0.01, //acercar con rueda mouse
                                    // sensitivity: 0.1,
                                    limits: {
                                                max: 10,
                                                min: 0.5
                                            }
                               }
                 }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
       }
    });



})


$(function () {
    var $NumEstuPorMunicipiodos = $("#NumEstuPorMunicipiodos");

    $.ajax({
        url: $NumEstuPorMunicipiodos.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiodos[0].getContext("2d");


        var PuntMatEstu = {
              label: 'Puntaje Prueba de Matematicas',
              backgroundColor: ['blue','red','orange'],
              //borderColor: ['red','yellow','green'],
              //borderWidth: 2,
              borderColor: 'Yellow',
              lineTension: 0,
              //fill: false,
              data:data.yPuntMatEstu
        };

        window.grafica2 = new Chart(ctx, {
          type: 'line',
          data: {
            labels: data.xAnios,
            datasets: [PuntMatEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Puntaje de Matematicas por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})

$(function () {
    var $NumEstuPorMunicipiotres = $("#NumEstuPorMunicipiotres");

    $.ajax({
        url: $NumEstuPorMunicipiotres.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiotres[0].getContext("2d");


        var PuntCienEstu = {
              label: 'Puntaje Prueba de Ciencias Naturales',
              //backgroundColor: 'blue',
              backgroundColor: ['blue','red','orange','green'],
              //borderColor: ['red','yellow','green'],
              borderWidth: 1,
              borderColor: ['red','blue','Yellow','orange'],
              lineTension: 0,
              fill: false,
              data:data.yPuntCienEstu
        };

        window.grafica3 = new Chart(ctx, {
          type: 'doughnut',
          data: {
            labels: data.xAnios,
            datasets: [PuntCienEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Puntaje Ciencias Naturales por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})


$(function () {
    var $NumEstuPorMunicipiocuatro = $("#NumEstuPorMunicipiocuatro");

    $.ajax({
        url: $NumEstuPorMunicipiocuatro.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiocuatro[0].getContext("2d");


        var PuntCompEstu = {
              label: 'Puntaje Prueba de Competencias',
              //backgroundColor: 'blue',
              backgroundColor: ['blue','red','orange','green'],
              //borderColor: ['red','yellow','green'],
              borderWidth: 1,
              borderColor: ['red','blue','Yellow','orange'],
              lineTension: 0,
              fill: false,
              data:data.yPuntCompEstu
        };

        window.grafica4 = new Chart(ctx, {
          type: 'radar',
          data: {
            labels: data.xAnios,
            datasets: [PuntCompEstu]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'Puntaje Competencias por Año'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                   }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})

// grafico BOXPLOT  o grafico VIOLIN


/*$(function () {
var $NumEstuPorMunicipiocinco = $("#NumEstuPorMunicipiocinco");


    //$.ajax({
      //url: $NumEstuPorMunicipiocinco.data("url"),
      //success: function (data) {
            //const ctx = $NumEstuPorMunicipiocinco[0].getContext("2d");

                                      var data = {
                                      labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
                                      datasets: [{
                                        label: 'Weekly Sales',
                                        outlierColor: '#999999',
                                        padding: 0,
                                        itemRadius: 3,
                                        //data: [18, 12, 6, 9, 12, 3, 9],
                                        data: [
                                            randomValues(100, 0, 100), // min wisker o vigote minimo
                                            randomValues(100, 0, 20), // Lower Quartile o cuartil inferior
                                            randomValues(100, 20, 70), // median o media
                                            randomValues(100, 60, 100), // Inter Quartil Range (IQR)
                                            randomValues(40, 50, 100), // mean o media
                                            randomValues(100, 60, 120), // Upper Quartil o cuartil superior
                                            randomValues(100, 80, 100), // max wisker o bigote maximo
                                        ],
                                        backgroundColor: 'rgba(255, 26, 104, 0.2)',
                                        borderColor: 'rgba(255, 26, 104, 1)',
                                        borderWidth: 1
                                      }]
                                    };

                                    // config
                                    var config = {
                                      type: 'boxplot',
                                      data,
                                      options: {
                                        scales: {
                                          y: {
                                            beginAtZero: true
                                          }
                                        }
                                      }
                                    };

                                    // render init block
                                    const myChart = new Chart(
                                      document.getElementById('myChart'),
                                      config
                                    );

                                    function randomValues(count, min, max){
                                        const delta = max - min;
                                        return Array.from({length: count}).map(() => Math.random() * delta + min);

                                     }

                                //}
           //});
    })  */


/* $(function () {
    var $NumEstuPorMunicipiocinco = $("#NumEstuPorMunicipiocinco");

    $.ajax({
        url: $NumEstuPorMunicipiocinco.data("url"),
      success: function (data) {

        var ctx = $NumEstuPorMunicipiocinco[0].getContext("2d");


        var Consulta = {
              label: 'Puntaje Prueba de Competencias',
              //backgroundColor: 'blue',
              backgroundColor: ['blue','red','orange'],
              //borderColor: ['red','yellow','green'],
              borderWidth: 1,
              borderColor: ['red','blue','Yellow'],
              lineTension: 0,
              fill: false,
              data:data.yConsulta
        };


        window.grafica5 = new Chart(ctx, {
          type: 'boxplot',
          data: {
            labels: data.xAnios,
            datasets: [{
                                        label: 'Weekly Sales',
                                        outlierColor: '#999999',
                                        padding: 0,
                                        itemRadius: 3,   */
                                        //data: [18, 12, 6, 9, 12, 3, 9],
                                   /*     data: [
                                            data.yPuntLengEstu,
                                            data.yPuntMatEstu,
                                            data.yPuntCienEstu,
                                            data.yPuntCompEstu, */
                                            //randomValues(100, 0, 100), // min wisker o vigote minimo
                                            //randomValues(100, 0, 20), // Lower Quartile o cuartil inferior
                                            //randomValues(100, 20, 70), // median o media
                                            //randomValues(100, 60, 100), // Inter Quartil Range (IQR)
                                            //randomValues(40, 50, 100), // mean o media
                                            //randomValues(100, 60, 120), // Upper Quartil o cuartil superior
                                            //randomValues(100, 80, 100), // max wisker o bigote maximo
                                /*        ],
                                        backgroundColor: 'rgba(255, 26, 104, 0.2)',
                                        borderColor: 'rgba(255, 26, 104, 1)',
                                        borderWidth: 1
                                      }]
          },
          options: {
                                        scales: {
                                          y: {
                                            beginAtZero: true
                                          }
                                        }
                    }
        });

        function randomValues(count, min, max)
        {
                const delta = max - min;
                return Array.from({length: count}).map(() => Math.random() * delta + min);

        }

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });

})*/