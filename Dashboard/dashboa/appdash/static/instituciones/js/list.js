$(function () {
    $('#data').DataTable({
        responsive: true,
        autoWidth: false,
        destroy: true,
        deferRender: true,
        ajax: {
            url: window.location.pathname,
            type: 'POST',
            data: {
                'action': 'searchdata'
            },
            dataSrc: ""
        },
        columns: [
            {"data": "id_institucion"},
            {"data": "ins_codigo_dane"},
            {"data": "ins_municipio"},
            {"data": "ins_id_ente"},
            {"data": "ins_nombre"},
            {"data": "ins_zona"},
            {"data": "ins_sector"},
            {"data": "ins_tipo_estab"},
            {"data": "ins_calendario"},
            {"data": "ins_nivel_socio"},
        ],
        columnDefs: [
            {
                targets: [],
                class: 'text-center',
                orderable: false,
                render: function (data, type, row) {
                }
            },
        ],
        initComplete: function (settings, json) {
        }
    });
});