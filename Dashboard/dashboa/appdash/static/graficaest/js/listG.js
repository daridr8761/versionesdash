$(function () {
    var $graficaest = $("#graficaest");
    $.ajax({
        url: $graficaest.data("url"),
      success: function (data) {

        var ctx = $graficaest[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              backgroundColor: '#3399FF',
              borderColor: 'blue',
              borderWidth: 1,
              //backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              data:data.ynum
        };


        window.grafica1 = new Chart(ctx, {
                  type: 'horizontalBar',
                  //type: 'pie',
                  //type: 'polarArea',
                  data: {
                            labels: data.xmuni,
                            datasets: [municipios]
                  },
                  options: {
                                responsive: true,
                                legend: {
                                  position: 'top',
                                },
                                title: {
                                  display: true,
                                  text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                                },
                                pan: {
                                            enabled: true,
                                            mode: "xy",
                                            speed: 10,
                                            threshold: 10
                                        },
                                zoom: {
                                            enabled: true,
                                            drag: false,
                                            mode: "xy",
                                            speed: 0.01, //acercar con rueda mouse
                                            // sensitivity: 0.1,
                                            limits: {
                                                        max: 10,
                                                        min: 0.5
                                                    }
                                       }
                            }
        });

        //var cont = data.ynum;
        //var nom = data.xmuni;
      }
    });
})


$(function () {
    var $graficaestdos = $("#graficaestdos");
    $.ajax({
        url: $graficaestdos.data("url"),
      success: function (data) {

        var ctx = $graficaestdos[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              backgroundColor: '#3399FF',
              borderColor: 'blue',
              borderWidth: 1,
              //backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              data:data.ynum
        };

        window.grafica2 = new Chart(ctx, {
          type: 'line',
          //type: 'pie',
          //type: 'polarArea',
          data: {
            labels: data.xmuni,
            datasets: [municipios]
          },
          options: {
                    responsive: true,
                    legend: {
                      position: 'top',
                    },
                    title: {
                      display: true,
                      text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                    },
                    pan: {
                                            enabled: true,
                                            mode: "xy",
                                            speed: 10,
                                            threshold: 10
                           },
                                zoom: {
                                            enabled: true,
                                            drag: false,
                                            mode: "xy",
                                            speed: 0.01, //acercar con rueda mouse
                                            // sensitivity: 0.1,
                                            limits: {
                                                        max: 10,
                                                        min: 0.5
                                                    }
                                        }
          }
        });
      }
    });
})



$(function () {
    var $graficaesttres = $("#graficaesttres");
    $.ajax({
        url: $graficaesttres.data("url"),
      success: function (data) {

        var ctx = $graficaesttres[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              //backgroundColor: '#3399FF',
              backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              borderColor: 'blue',
              borderWidth: 1,
              data:data.ynum
        };

        window.grafica3 = new Chart(ctx, {
          type: 'pie',
          //type: 'pie',
          //type: 'polarArea',
          data: {
            labels: data.xmuni,
            datasets: [municipios]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                    }
        });
      }
    });
})

$(function () {
    var $graficaestcuatro = $("#graficaestcuatro");
    $.ajax({
        url: $graficaestcuatro.data("url"),
      success: function (data) {

        var ctx = $graficaestcuatro[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              //backgroundColor: '#3399FF',
              backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              borderColor: 'blue',
              borderWidth: 1,
              data:data.ynum
        };

        window.grafica4 = new Chart(ctx, {
          type: 'radar',
          //type: 'pie',
          //type: 'polarArea',
          data: {
            labels: data.xmuni,
            datasets: [municipios]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                    }
        });
      }
    });
})

$(function () {
    var $graficaest_B = $("#graficaest_B");
    $.ajax({
        url: $graficaest_B.data("url"),
      success: function (data) {

        var ctx = $graficaest_B[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              backgroundColor: '#3399FF',
              borderColor: 'blue',
              borderWidth: 1,
              //backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              data:data.ynum
        };


        window.grafica5 = new Chart(ctx, {
                  type: 'horizontalBar',
                  //type: 'pie',
                  //type: 'polarArea',
                  data: {
                            labels: data.xmuni,
                            datasets: [municipios]
                  },
                  options: {
                                responsive: true,
                                legend: {
                                  position: 'top',
                                },
                                title: {
                                  display: true,
                                  text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                                },
                                pan: {
                                            enabled: true,
                                            mode: "xy",
                                            speed: 10,
                                            threshold: 10
                                        },
                                zoom: {
                                            enabled: true,
                                            drag: false,
                                            mode: "xy",
                                            speed: 0.01, //acercar con rueda mouse
                                            // sensitivity: 0.1,
                                            limits: {
                                                        max: 10,
                                                        min: 0.5
                                                    }
                                       }
                            }
        });
      }
    });
})


$(function () {
    var $graficaestdos_L = $("#graficaestdos_L");
    $.ajax({
        url: $graficaestdos_L.data("url"),
      success: function (data) {

        var ctx = $graficaestdos_L[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              backgroundColor: '#3399FF',
              borderColor: 'blue',
              borderWidth: 1,
              //backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              data:data.ynum
        };

        window.grafica6 = new Chart(ctx, {
          type: 'line',
          //type: 'pie',
          //type: 'polarArea',
          data: {
            labels: data.xmuni,
            datasets: [municipios]
          },
          options: {
                    responsive: true,
                    legend: {
                      position: 'top',
                    },
                    title: {
                      display: true,
                      text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                    },
                    pan: {
                                            enabled: true,
                                            mode: "xy",
                                            speed: 10,
                                            threshold: 10
                           },
                                zoom: {
                                            enabled: true,
                                            drag: false,
                                            mode: "xy",
                                            speed: 0.01, //acercar con rueda mouse
                                            // sensitivity: 0.1,
                                            limits: {
                                                        max: 10,
                                                        min: 0.5
                                                    }
                                        }
          }
        });
      }
    });
})

$(function () {
    var $graficaesttres_P = $("#graficaesttres_P");
    $.ajax({
        url: $graficaesttres_P.data("url"),
      success: function (data) {

        var ctx = $graficaesttres_P[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              //backgroundColor: '#3399FF',
              backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              borderColor: 'blue',
              borderWidth: 1,
              data:data.ynum
        };

        window.grafica7 = new Chart(ctx, {
          type: 'pie',
          //type: 'pie',
          //type: 'polarArea',
          data: {
            labels: data.xmuni,
            datasets: [municipios]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                                    zoom: {
                                                enabled: true,
                                                drag: false,
                                                mode: "xy",
                                                speed: 0.01, //acercar con rueda mouse
                                                // sensitivity: 0.1,
                                                limits: {
                                                            max: 10,
                                                            min: 0.5
                                                        }
                                            }
                    }
        });
      }
    });
})


$(function () {
    var $graficaestcuatro_R = $("#graficaestcuatro_R");
    $.ajax({
        url: $graficaestcuatro_R.data("url"),
      success: function (data) {

        var ctx = $graficaestcuatro_R[0].getContext("2d");

        var municipios = {
          label: 'Estudiantes',
              //backgroundColor: '#3399FF',
              backgroundColor: ['yellow','blue','red','orange','black','green','pink','violet','purple','golden','brown','silver','gray'],
              borderColor: 'blue',
              borderWidth: 1,
              data:data.ynum
        };

        new Chart(ctx, {
          type: 'radar',
          //type: 'pie',
          //type: 'polarArea',
          data: {
            labels: data.xmuni,
            datasets: [municipios]
          },
          options: {
                        responsive: true,
                        legend: {
                          position: 'top',
                        },
                        title: {
                          display: true,
                          text: 'NUMERO DE ESTUDIANTES POR MUNICIPIO'
                        },
                        pan: {
                                                enabled: true,
                                                mode: "xy",
                                                speed: 10,
                                                threshold: 10
                               },
                    }
        });
      }
    });
})