from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from django.db.models import Sum, Count, Q
from appdash.forms import EstudianteForm
import psycopg2

'''class NumEstuPorMunicipioListView(ListView):
    model = DimEstudiante
    #model = DimEstudiante
    #model = DimInstituciones
    #model = DimMunicipios
    template_name = 'NumEstuPorTipoEsta/list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in FactSaber5.objects.values_list('muni_id_ente__muni_nombre','id_institucion__ins_tipo_estab').annotate(Sum('numero_estu')).order_by('id_institucion__ins_tipo_estab'):
                    data.append(i.toJSON())
                else:
                    data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)


    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Consultas'
        context['create_url'] = reverse_lazy('appdash:estudiante_create')
        context['list_url'] = reverse_lazy('appdash:NumEstuPorMunicipio_list')
        # la anterior variable create_url nos lleva a la ruta de la vista estudiante_create donde la llamemos
        context['entity'] = 'Estudiantes Por Municipio'
        # la anterior variable entity nos muestra el nombre Estudiantes donde la llamemos
        return context '''

# esta vista funciona bien pero sale en el template como diccionarios y no puedo coger las posiciones de cada columna en el template
'''class NumEstuPorMunicipioListView(ListView):
    model = DimEstudiante
    template_name = 'NumEstuPorTipoEsta/list.html'

    def get_context_data(self, **kwargs):
        conexion1 = psycopg2.connect(database="data_mart_5", user="postgres", password="Dario3761410")
        cursor1 = conexion1.cursor()
        cursor1.execute("SELECT muni_nombre, ins_tipo_estab, SUM(numero_estu) FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) JOIN dim_instituciones USING(id_institucion) GROUP BY 1,2 ORDER BY 1,2")
        data = {
          "title":     "Listado de Consulta",
          "categories": cursor1
        }
        return data'''

'''def NumEstuPorMunicipioListView(request):
    conexion1 = psycopg2.connect(database="data_mart_5", user="postgres", password="Dario3761410")
    cursor1 = conexion1.cursor()
    cursor1.execute("SELECT muni_nombre, ins_tipo_estab, SUM(numero_estu) FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) JOIN dim_instituciones USING(id_institucion) GROUP BY 1,2 ORDER BY 1,2")
    #names = [ row[0] for row in cursor1]
    for fil in cursor1:
        data = {
            "title": "Listado de Consulta",
             "cat": fil
         }
        #print(data['cat'][0],data['cat'][1],data['cat'][2])
    #conexion1.close()
    
    return render(request,'NumEstuPorTipoEsta/list.html', data)'''


'''def NumEstuPorMunicipioListView(request):
    qs1 = DimEstudiante.objects.values('id_estudiante','estu_sexo')
    qs2 = FactSaber5.objects.values('puntaje_matematicas')
    qr3 = qs1.intersection(qs2).order_by('estu_sexo')
    data = {
            'title': 'Listado Municipios',
            'categories': qr3
    }
    return render(request,'NumEstuPorTipoEsta/list.html', data)'''


def NumEstuPorMunicipioListView(request):
    title = "Consulta Numero de Estudiantes por Municicio y Año de Presentacion de Todas las Materias Evaluadas"
    list_url = reverse_lazy('appdash:NumEstuPorMunicipio_list')
    entity = 'Consulta'
    from django.db import connection
    with connection.cursor() as cursor:
        #cursor.execute("SELECT muni_nombre, ins_tipo_estab, SUM(numero_estu) as suma FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) JOIN dim_instituciones USING(id_institucion) GROUP BY 1,2 ORDER BY 1,2")
        cursor.execute("SELECT muni_nombre, anio, COUNT(id_estudiante) as suma FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) GROUP BY 1,2 ORDER BY 1,2")
        #cursor.execute("SELECT muni_nombre, ROUND(AVG(puntaje_lenguaje),2) AS PROMEDIO FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) GROUP BY 1 ORDER BY 1")
        results = cursor.fetchall()

        #rawData = list(cursor.fetchall())
        #rawData = namedtuplefetchall(cursor)
        #print(rawData[0][1])
        #result = []
        #for r in rawData:
         #   result.append(list(r))
        contexto = {"consultas": results,"title": title, "list_url": list_url, "entity": entity}
    return render(request,'NumEstuPorMunicipio/list.html', contexto)

def Num_Estu_Por_Municipio(request):
    dataAnios = []
    SumasU = []
    SumEstu = []
    SumasO = []
    TipEsta = []
    try:
        #for i in FactSaber5.objects.values('muni_id_ente__muni_nombre').annotate(Suma=Count('numero_estu')).filter(id_institucion__ins_tipo_estab='OFICIAL_RURAL'):
        #    Munidata.append(i['muni_id_ente__muni_nombre'])
        #    SumasR.append(i['Suma'])
        #for i in FactSaber5.objects.values('muni_id_ente__muni_nombre').annotate(Suma=Count('numero_estu')).filter(id_institucion__ins_tipo_estab='OFICIAL_URBANO'):
        #    SumasU.append(i['Suma'])
        #for i in FactSaber5.objects.values('muni_id_ente__muni_nombre').annotate(Suma=Count('numero_estu')).filter(id_institucion__ins_tipo_estab='NO_OFICIAL'):
        #    SumasO.append(i['Suma'])
        ''' for i in FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
            tipo1=Count('id_estudiante', filter=Q(anio="2014")),
            # filter=Q es un if dentro de la consulta
            tipo3=Count('id_estudiante', filter=Q(anio="2015")),
            tipo2=Count('id_estudiante', filter=Q(anio="2016"))).order_by(
            'muni_id_ente__muni_nombre', 'anio'):
            Munidata.append(i['muni_id_ente__muni_nombre'] + " : " + i['anio'])
            # p_hombres.append(entry['prom1'])
            # p_mujeres.append(entry['prom2'])
            # no_espe.append(entry['prom3'])
            SumasR.append(i['tipo1'])
            SumasU.append(i['tipo3'])
            SumasO.append(i['tipo2']) '''

        result = FactSaber5.objects.values('muni_id_ente__muni_nombre','anio').annotate(suma=Count('id_estudiante')).order_by('muni_id_ente__muni_nombre',
            'anio')
        for i in result:
            dataAnios.append(i['muni_id_ente__muni_nombre'] + " : " + i['anio'])
            SumEstu.append(i['suma'])

    except Exception as e:
        dataAnios['error'] = str(e)
        SumEstu['error'] = str(e)
        SumasU['error'] = str(e)
        SumasO['error'] = str(e)
    return JsonResponse(data={
        'xAnios': dataAnios,
        'yEstu': SumEstu,
        'yU': SumasU,
        'yO': SumasO,
    })

'''class NumEstuPorMunicipioListView(ListView):
        model = DimEstudiante
        template_name = 'NumEstuPorTipoEsta/list.html'

        @method_decorator(csrf_exempt)
        def dispatch(self, request, *args, **kwargs):
            return super().dispatch(request, *args, **kwargs)

        def post(self, request, *args, **kwargs):
            from django.db import connection
            with connection.cursor() as cursor:
                cursor.execute("SELECT muni_nombre, ins_tipo_estab, SUM(numero_estu) FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) JOIN dim_instituciones USING(id_institucion) GROUP BY 1,2 ORDER BY 1,2")
                rawData = cursor.fetchall()
                result = []
                for r in rawData:
                    result.append(list(r))
                contexto = {"consultas": result}
            return JsonResponse(contexto, safe=False)


        def get_context_data(self, **kwargs):
            context = super().get_context_data(**kwargs)
            context['title'] = 'Listado de Consultas'
            context['create_url'] = reverse_lazy('appdash:estudiante_create')
            context['list_url'] = reverse_lazy('appdash:NumEstuPorMunicipio_list')
            # la anterior variable create_url nos lleva a la ruta de la vista estudiante_create donde la llamemos
            context['entity'] = 'Estudiantes Por Municipio'
            # la anterior variable entity nos muestra el nombre Estudiantes donde la llamemos
            return context'''




