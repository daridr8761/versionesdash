from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from appdash.forms import EstudianteForm
from django.db.models import Count, Avg, Sum, Func, Q

class Round(Func):
 function = 'ROUND'
 template='%(function)s(%(expressions)s, 2)'

def DashboardListViewGeneral(request):

    label = []
    dato = []

    p_hombres = []
    p_mujeres = []
    p_no_espe = []
    hombres = []
    mujeres = []
    no_espe = []

    oficial_r = []
    no_oficial = []
    oficial_u = []
    p_oficial_r = []
    p_no_oficial = []
    p_oficial_u = []

    p_rural = []
    p_urbano = []
    rural = []
    urbano = []

    p_con_discapacidad = []
    p_sin_discapacidad = []
    con_discapacidad = []
    sin_discapacidad = []

    #estudiante = request.GET['estudiante']
    municipio = request.GET['municipio']
    inst = request.GET['inst']
    anios = request.GET['anios']
    cat = request.GET['cat']
    puntaje = request.GET['punt']

    if (anios == "TODOS"):
      if (municipio == "TODOS"):
          if (cat== "Genero"):
              result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
              prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="MASCULINO"))),
              prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="FEMENINO"))),
              prom3=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))),
              conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")), #filter=Q es un if dentro de la consulta
              conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
              conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).order_by(
              'muni_id_ente__muni_nombre', 'anio')
              for entry in result1:
                  label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                  p_hombres.append(entry['prom1'])
                  p_mujeres.append(entry['prom2'])
                  p_no_espe.append(entry['prom3'])
                  hombres.append(entry['conta1'])
                  mujeres.append(entry['conta2'])
                  no_espe.append(entry['conta3'])
          if (cat=='Tipo de Establecimiento'):
                  result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                  prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL"))),
                  prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL"))),
                  prom3=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))),
                  conta1=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),  # filter=Q es un if dentro de la consulta
                  conta2=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                  conta3=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).order_by(
                  'muni_id_ente__muni_nombre', 'anio')
                  for entry in result1:
                      label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                      p_oficial_r.append(entry['prom1'])
                      p_no_oficial.append(entry['prom2'])
                      p_oficial_u.append(entry['prom3'])
                      oficial_r.append(entry['conta1'])
                      no_oficial.append(entry['conta2'])
                      oficial_u.append(entry['conta3'])
          if (cat=='Zona'):
              result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
              prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="RURAL"))),
              prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="URBANO"))),
              conta1=Count('id_estudiante', filter=Q(id_institucion__ins_zona="RURAL")),  # filter=Q es un if dentro de la consulta
              conta2=Count('id_estudiante', filter=Q(id_institucion__ins_zona="URBANO"))).order_by(
              'muni_id_ente__muni_nombre', 'anio')
              for entry in result1:
                  label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                  p_rural.append(entry['prom1'])
                  p_urbano.append(entry['prom2'])
                  rural.append(entry['conta1'])
                  urbano.append(entry['conta2'])
          else:
              if (cat=='Discapacidad Cognitiva'):
                  result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                  prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD"))),
                  prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))),
                  conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),  # filter=Q es un if dentro de la consulta
                  conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).order_by(
                  'muni_id_ente__muni_nombre', 'anio')
                  for entry in result1:
                      label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                      p_con_discapacidad.append(entry['prom1'])
                      p_sin_discapacidad.append(entry['prom2'])
                      con_discapacidad.append(entry['conta1'])
                      sin_discapacidad.append(entry['conta2'])
      else:
          if (inst == "General"):
              if (cat == "Genero"):
                  result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="MASCULINO"))),
                      prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="FEMENINO"))),
                      prom3=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))),
                      conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                      conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                      conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                      'id_institucion__ins_nombre', 'anio') # se filtra por genero y por el nombre del municipio que elija el usuario
                  for entry in result1:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                          'anio'])  # guarda nombre del departamento ya agrupado
                      p_hombres.append(entry['prom1'])
                      p_mujeres.append(entry['prom2'])
                      p_no_espe.append(entry['prom3'])
                      hombres.append(entry['conta1'])
                      mujeres.append(entry['conta2'])
                      no_espe.append(entry['conta3'])
              if (cat == 'Tipo de Establecimiento'):
                      result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL"))),
                          prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL"))),
                          prom3=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))),
                          conta1=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),# filter=Q es un if dentro de la consulta
                          conta2=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                          conta3=Count('id_estudiante',filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                          'id_institucion__ins_nombre', 'anio')
                      for entry in result1:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                          p_oficial_r.append(entry['prom1'])
                          p_no_oficial.append(entry['prom2'])
                          p_oficial_u.append(entry['prom3'])
                          oficial_r.append(entry['conta1'])
                          no_oficial.append(entry['conta2'])
                          oficial_u.append(entry['conta3'])
              if (cat == 'Zona'):
                  result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="RURAL"))),
                      prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="URBANO"))),
                      conta1=Count('id_estudiante', filter=Q(id_institucion__ins_zona="RURAL")),# filter=Q es un if dentro de la consulta
                      conta2=Count('id_estudiante',filter=Q(id_institucion__ins_zona="URBANO"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                      'id_institucion__ins_nombre', 'anio')
                  for entry in result1:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                      p_rural.append(entry['prom1'])
                      p_urbano.append(entry['prom2'])
                      rural.append(entry['conta1'])
                      urbano.append(entry['conta2'])
              else:
                  if (cat == 'Discapacidad Cognitiva'):
                      result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD"))),
                          prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))),
                          conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),# filter=Q es un if dentro de la consulta
                          conta2=Count('id_estudiante',filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                          'id_institucion__ins_nombre', 'anio')
                      for entry in result1:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                          p_con_discapacidad.append(entry['prom1'])
                          p_sin_discapacidad.append(entry['prom2'])
                          con_discapacidad.append(entry['conta1'])
                          sin_discapacidad.append(entry['conta2'])
          else:
              if (cat == "Genero"):
                  results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="MASCULINO"))),
                      prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="FEMENINO"))),
                      prom3=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))),
                      conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                      conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                      conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(muni_id_ente__muni_nombre=municipio,
                      id_institucion__ins_nombre=inst).order_by('id_institucion__ins_nombre', 'anio')
                  for entry in results:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                          'anio'])  # guarda nombre del departamento ya agrupado
                      p_hombres.append(entry['prom1'])
                      p_mujeres.append(entry['prom2'])
                      p_no_espe.append(entry['prom3'])
                      hombres.append(entry['conta1'])
                      mujeres.append(entry['conta2'])
                      no_espe.append(entry['conta3'])
              if (cat == 'Tipo de Establecimiento'):
                      result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL"))),
                          prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL"))),
                          prom3=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))),
                          conta1=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),
                          # filter=Q es un if dentro de la consulta
                          conta2=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                          conta3=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(muni_id_ente__muni_nombre=municipio,
                          id_institucion__ins_nombre=inst).order_by('id_institucion__ins_nombre', 'anio')
                      for entry in result1:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                          p_oficial_r.append(entry['prom1'])
                          p_no_oficial.append(entry['prom2'])
                          p_oficial_u.append(entry['prom3'])
                          oficial_r.append(entry['conta1'])
                          no_oficial.append(entry['conta2'])
                          oficial_u.append(entry['conta3'])
              if (cat == 'Zona'):
                  result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="RURAL"))),
                      prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="URBANO"))),
                      conta1=Count('id_estudiante', filter=Q(id_institucion__ins_zona="RURAL")),# filter=Q es un if dentro de la consulta
                      conta2=Count('id_estudiante', filter=Q(id_institucion__ins_zona="URBANO"))).filter(muni_id_ente__muni_nombre=municipio,
                      id_institucion__ins_nombre=inst).order_by('id_institucion__ins_nombre', 'anio')
                  for entry in result1:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                      p_rural.append(entry['prom1'])
                      p_urbano.append(entry['prom2'])
                      rural.append(entry['conta1'])
                      urbano.append(entry['conta2'])
              else:
                  if (cat == 'Discapacidad Cognitiva'):
                      result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD"))),
                          prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))),
                          conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),# filter=Q es un if dentro de la consulta
                          conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(muni_id_ente__muni_nombre=municipio,
                          id_institucion__ins_nombre=inst).order_by('id_institucion__ins_nombre', 'anio')
                      for entry in result1:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                          p_con_discapacidad.append(entry['prom1'])
                          p_sin_discapacidad.append(entry['prom2'])
                          con_discapacidad.append(entry['conta1'])
                          sin_discapacidad.append(entry['conta2'])
    else:
        if (municipio == "TODOS"):
            if (cat == "Genero"):
                results = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                   prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="MASCULINO"))),
                   prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="FEMENINO"))),
                   prom3=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))),
                   conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")), #filter=Q es un if dentro de la consulta
                   conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                   conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(anio=anios).order_by('muni_id_ente__muni_nombre', 'anio')
                for entry in results:
                    label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry[
                        'anio'])  # guarda nombre del departamento ya agrupado
                    p_hombres.append(entry['prom1'])
                    p_mujeres.append(entry['prom2'])
                    p_no_espe.append(entry['prom3'])
                    hombres.append(entry['conta1'])
                    mujeres.append(entry['conta2'])
                    no_espe.append(entry['conta3'])
            if (cat == 'Tipo de Establecimiento'):
                    result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                        prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL"))),
                        prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL"))),
                        prom3=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))),
                        conta1=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),# filter=Q es un if dentro de la consulta
                        conta2=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                        conta3=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(anio=anios).order_by(
                        'muni_id_ente__muni_nombre', 'anio')
                    for entry in result1:
                        label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                        p_oficial_r.append(entry['prom1'])
                        p_no_oficial.append(entry['prom2'])
                        p_oficial_u.append(entry['prom3'])
                        oficial_r.append(entry['conta1'])
                        no_oficial.append(entry['conta2'])
                        oficial_u.append(entry['conta3'])
            if (cat == 'Zona'):
                result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                    prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="RURAL"))),
                    prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="URBANO"))),
                    conta1=Count('id_estudiante', filter=Q(id_institucion__ins_zona="RURAL")),# filter=Q es un if dentro de la consulta
                    conta2=Count('id_estudiante', filter=Q(id_institucion__ins_zona="URBANO"))).filter(anio=anios).order_by(
                    'muni_id_ente__muni_nombre', 'anio')
                for entry in result1:
                    label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                    p_rural.append(entry['prom1'])
                    p_urbano.append(entry['prom2'])
                    rural.append(entry['conta1'])
                    urbano.append(entry['conta2'])
            else:
                if (cat == 'Discapacidad Cognitiva'):
                    result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                        prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD"))),
                        prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))),
                        conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),# filter=Q es un if dentro de la consulta
                        conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(anio=anios).order_by(
                        'muni_id_ente__muni_nombre', 'anio')
                    for entry in result1:
                        label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                        p_con_discapacidad.append(entry['prom1'])
                        p_sin_discapacidad.append(entry['prom2'])
                        con_discapacidad.append(entry['conta1'])
                        sin_discapacidad.append(entry['conta2'])
        else:
            if (inst == "General"):
                if (cat == "Genero"):
                    results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                        prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="MASCULINO"))),
                        prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="FEMENINO"))),
                        prom3=Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))),
                        conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                        conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                        conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(muni_id_ente__muni_nombre=municipio, anio=anios).order_by(
                        'id_institucion__ins_nombre', 'anio')
                    for entry in results:
                        label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                            'anio'])  # guarda nombre del departamento ya agrupado
                        p_hombres.append(entry['prom1'])
                        p_mujeres.append(entry['prom2'])
                        p_no_espe.append(entry['prom3'])
                        hombres.append(entry['conta1'])
                        mujeres.append(entry['conta2'])
                        no_espe.append(entry['conta3'])
                if (cat == 'Tipo de Establecimiento'):
                        result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                            prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL"))),
                            prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL"))),
                            prom3=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))),
                            conta1=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),# filter=Q es un if dentro de la consulta
                            conta2=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                            conta3=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(muni_id_ente__muni_nombre=municipio, anio=anios).order_by(
                            'id_institucion__ins_nombre', 'anio')
                        for entry in result1:
                            label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                            p_oficial_r.append(entry['prom1'])
                            p_no_oficial.append(entry['prom2'])
                            p_oficial_u.append(entry['prom3'])
                            oficial_r.append(entry['conta1'])
                            no_oficial.append(entry['conta2'])
                            oficial_u.append(entry['conta3'])
                if (cat == 'Zona'):
                    result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                        prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="RURAL"))),
                        prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="URBANO"))),
                        conta1=Count('id_estudiante', filter=Q(id_institucion__ins_zona="RURAL")),# filter=Q es un if dentro de la consulta
                        conta2=Count('id_estudiante', filter=Q(id_institucion__ins_zona="URBANO"))).filter(muni_id_ente__muni_nombre=municipio, anio=anios).order_by(
                        'id_institucion__ins_nombre', 'anio')
                    for entry in result1:
                        label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                        p_rural.append(entry['prom1'])
                        p_urbano.append(entry['prom2'])
                        rural.append(entry['conta1'])
                        urbano.append(entry['conta2'])
                else:
                    if (cat == 'Discapacidad Cognitiva'):
                        result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                            prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD"))),
                            prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))),
                            conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),# filter=Q es un if dentro de la consulta
                            conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(muni_id_ente__muni_nombre=municipio, anio=anios).order_by(
                            'id_institucion__ins_nombre', 'anio')
                        for entry in result1:
                            label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                            p_con_discapacidad.append(entry['prom1'])
                            p_sin_discapacidad.append(entry['prom2'])
                            con_discapacidad.append(entry['conta1'])
                            sin_discapacidad.append(entry['conta2'])
            else:
                if (cat == "Genero"):
                    results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                        prom1 = Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="MASCULINO"))),
                        prom2 = Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="FEMENINO"))),
                        prom3 = Round(Avg(puntaje, filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))),
                        conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                        conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                        conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(muni_id_ente__muni_nombre=municipio,
                        id_institucion__ins_nombre=inst, anio=anios).order_by('id_institucion__ins_nombre', 'anio')
                    for entry in results:
                        label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                            'anio'])  # guarda nombre del departamento ya agrupado
                        p_hombres.append(entry['prom1'])
                        p_mujeres.append(entry['prom2'])
                        p_no_espe.append(entry['prom3'])
                        hombres.append(entry['conta1'])
                        mujeres.append(entry['conta2'])
                        no_espe.append(entry['conta3'])
                if (cat == 'Tipo de Establecimiento'):
                        result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                            prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL"))),
                            prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL"))),
                            prom3=Round(Avg(puntaje, filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))),
                            conta1=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),# filter=Q es un if dentro de la consulta
                            conta2=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                            conta3=Count('id_estudiante', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(muni_id_ente__muni_nombre=municipio,
                            id_institucion__ins_nombre=inst, anio=anios).order_by('id_institucion__ins_nombre', 'anio')
                        for entry in result1:
                            label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                            p_oficial_r.append(entry['prom1'])
                            p_no_oficial.append(entry['prom2'])
                            p_oficial_u.append(entry['prom3'])
                            oficial_r.append(entry['conta1'])
                            no_oficial.append(entry['conta2'])
                            oficial_u.append(entry['conta3'])
                if (cat == 'Zona'):
                    result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                        prom1=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="RURAL"))),
                        prom2=Round(Avg(puntaje, filter=Q(id_institucion__ins_zona="URBANO"))),
                        conta1=Count('id_estudiante', filter=Q(id_institucion__ins_zona="RURAL")),# filter=Q es un if dentro de la consulta
                        conta2=Count('id_estudiante', filter=Q(id_institucion__ins_zona="URBANO"))).filter(muni_id_ente__muni_nombre=municipio,
                        id_institucion__ins_nombre=inst, anio=anios).order_by('id_institucion__ins_nombre', 'anio')
                    for entry in result1:
                        label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                        p_rural.append(entry['prom1'])
                        p_urbano.append(entry['prom2'])
                        rural.append(entry['conta1'])
                        urbano.append(entry['conta2'])
                else:
                    if (cat == 'Discapacidad Cognitiva'):
                        result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                            prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD"))),
                            prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))),
                            conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),# filter=Q es un if dentro de la consulta
                            conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(muni_id_ente__muni_nombre=municipio,
                            id_institucion__ins_nombre=inst, anio=anios).order_by('id_institucion__ins_nombre', 'anio')
                        for entry in result1:
                            label.append(entry['id_institucion__ins_nombre'] + " : " + entry['anio'])
                            p_con_discapacidad.append(entry['prom1'])
                            p_sin_discapacidad.append(entry['prom2'])
                            con_discapacidad.append(entry['conta1'])
                            sin_discapacidad.append(entry['conta2'])


    return JsonResponse(data={

        'labels': label,
        'data': dato,

        'p_hombres': p_hombres,
        'p_mujeres': p_mujeres,
        'p_no_espe': p_no_espe,
        'hombres': hombres,
        'mujeres': mujeres,
        'no_espe': no_espe,

        'p_oficial_r': p_oficial_r,
        'p_no_oficial': p_no_oficial,
        'p_oficial_u': p_oficial_u,
        'oficial_r': oficial_r,
        'no_oficial': no_oficial,
        'oficial_u': oficial_u,

        'p_rural': p_rural,
        'p_urbano': p_urbano,
        'rural': rural,
        'urbano': urbano,

        'p_con_discapacidad': p_con_discapacidad,
        'p_sin_discapacidad': p_sin_discapacidad,
        'con_discapacidad': con_discapacidad,
        'sin_discapacidad': sin_discapacidad,


    })

def DashboardHtmlGeneral(request):
    title = "Consulta Dashboard >> Numero de Estudiantes y Puntajes"
    list_url = reverse_lazy('appdash:dashboard_list1G')
    entity = 'Consulta'

    contexto = {"title": title, "list_url": list_url, "entity": entity}

    return render(request, "dashboardGeneral/list.html", contexto)