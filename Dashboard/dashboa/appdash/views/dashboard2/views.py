from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from appdash.forms import EstudianteForm
from django.db.models import Count, Avg, Sum, Func, Q

class Round(Func):
 function = 'ROUND'
 template='%(function)s(%(expressions)s, 2)'

def DashboardListView(request):

    label = []
    dato = []
    #p_hombres = []
    #p_mujeres = []
    #p_no_espe = []
    hombres = []
    mujeres = []
    no_espe = []

    oficial_r = []
    no_oficial = []
    oficial_u = []

    rural = []
    urbano = []

    con_discapacidad = []
    sin_discapacidad = []

    insuficiente = []
    minimo = []
    satisfactorio = []
    avanzado = []


    #estudiante = request.GET['estudiante']
    municipio = request.GET['municipio']
    inst = request.GET['inst']
    anios = request.GET['anios']
    cat = request.GET['cat']

    if (anios == "TODOS"):
      if (municipio == "TODOS"):
          if (cat== "Genero"):
              result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                  #prom1=Round(Avg(puntaje, filter=Q(id_estudiante__estu_genero="MASCULINO"))),
                  #prom2=Round(Avg(puntaje, filter=Q(id_estudiante__estu_genero="FEMENINO"))),
                  #prom3=Round(Avg(puntaje, filter=Q(id_estudiante__estu_genero="NO_ESPECIFICA"))),
              conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")), #filter=Q es un if dentro de la consulta
              conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
              conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).order_by(
              'muni_id_ente__muni_nombre', 'anio')
              for entry in result1:
                  label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                  #p_hombres.append(entry['prom1'])
                  #p_mujeres.append(entry['prom2'])
                  #no_espe.append(entry['prom3'])
                  hombres.append(entry['conta1'])
                  mujeres.append(entry['conta2'])
                  no_espe.append(entry['conta3'])
          if(cat == "Tipo de Establecimiento"):
              result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                  tipo1=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),
                  # filter=Q es un if dentro de la consulta
                  tipo2=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                  tipo3=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).order_by(
                  'muni_id_ente__muni_nombre', 'anio')
              for entry in result1:
                  label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                  # p_hombres.append(entry['prom1'])
                  # p_mujeres.append(entry['prom2'])
                  # no_espe.append(entry['prom3'])
                  oficial_r.append(entry['tipo1'])
                  no_oficial.append(entry['tipo2'])
                  oficial_u.append(entry['tipo3'])
          if(cat == "Zona"):
                  result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                      zona1=Count('id_institucion', filter=Q(id_institucion__ins_zona="RURAL")), # filter=Q es un if dentro de la consulta
                      zona2=Count('id_institucion', filter=Q(id_institucion__ins_zona="URBANO"))).order_by(
                      'muni_id_ente__muni_nombre', 'anio')
                  for entry in result1:
                      label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                      rural.append(entry['zona1'])
                      urbano.append(entry['zona2'])
          if(cat == "Discapacidad Cognitiva"):
              result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                  con=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")), # filter=Q es un if dentro de la consulta
                  sin=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).order_by(
                  'muni_id_ente__muni_nombre', 'anio')
              for entry in result1:
                  label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                  con_discapacidad.append(entry['con'])
                  sin_discapacidad.append(entry['sin'])
          else:
              if(cat == "Nivel de Desempeño Global"):
                  result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                      ins=Count('id_estudiante', filter=Q(desempenio="INSUFICIENTE")), # filter=Q es un if dentro de la consulta
                      min=Count('id_estudiante', filter=Q(desempenio="MINIMO")),
                      satis=Count('id_estudiante', filter=Q(desempenio="SATISFACTORIO")),
                      avan=Count('id_estudiante', filter=Q(desempenio="AVANZADO"))).order_by(
                      'muni_id_ente__muni_nombre', 'anio')
                  for entry in result1:
                      label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                      insuficiente.append(entry['ins'])
                      minimo.append(entry['min'])
                      satisfactorio.append(entry['satis'])
                      avanzado.append(entry['avan'])

      else:
          if (inst == "General"):
              if (cat == "Genero"):
                  result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                      conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                      conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                      'id_institucion__ins_nombre', 'anio') # se filtra por genero y por el nombre del municipio que elija el usuario
                  for entry in result1:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                          'anio'])  # guarda nombre del departamento ya agrupado
                      hombres.append(entry['conta1'])
                      mujeres.append(entry['conta2'])
                      no_espe.append(entry['conta3'])
              if (cat == "Tipo de Establecimiento"):
                  results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      tipo1=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),
                      tipo2=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                      tipo3=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                      'id_institucion__ins_nombre', 'anio')
                  for entry in results:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                          'anio'])  # guarda nombre del departamento ya agrupado
                      oficial_r.append(entry['tipo1'])
                      no_oficial.append(entry['tipo2'])
                      oficial_u.append(entry['tipo3'])
              if(cat == "Zona"):
                      results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          zona1=Count('id_institucion', filter=Q(id_institucion__ins_zona="RURAL")),
                          zona2=Count('id_institucion', filter=Q(id_institucion__ins_zona="URBANO"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                          'id_institucion__ins_nombre', 'anio')
                      for entry in results:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                              'anio'])  # guarda nombre del departamento ya agrupado
                          rural.append(entry['zona1'])
                          urbano.append(entry['zona2'])
              if(cat == "Discapacidad Cognitiva"):
                  results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      con=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),
                      sin=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                      'id_institucion__ins_nombre', 'anio')
                  for entry in results:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                          'anio'])  # guarda nombre del departamento ya agrupado
                      con_discapacidad.append(entry['con'])
                      sin_discapacidad.append(entry['sin'])
              else:
                  if(cat == "Nivel de Desempeño Global"):
                      results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          ins=Count('id_estudiante', filter=Q(desempenio="INSUFICIENTE")),
                          min=Count('id_estudiante', filter=Q(desempenio="MINIMO")),
                          satis=Count('id_estudiante', filter=Q(desempenio="SATISFACTORIO")),
                          avan=Count('id_estudiante', filter=Q(desempenio="AVANZADO"))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                          'id_institucion__ins_nombre', 'anio')
                      for entry in results:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                              'anio'])  # guarda nombre del departamento ya agrupado
                          insuficiente.append(entry['ins'])
                          minimo.append(entry['min'])
                          satisfactorio.append(entry['satis'])
                          avanzado.append(entry['avan'])
          else:
              if (cat == "Genero"):
                  result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                      conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                      conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(muni_id_ente__muni_nombre=municipio, id_institucion__ins_nombre=inst).order_by(
                      'id_institucion__ins_nombre', 'anio') # se filtra por genero y por el nombre del municipio que elija el usuario
                  for entry in result1:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                          'anio'])  # guarda nombre del departamento ya agrupado
                      hombres.append(entry['conta1'])
                      mujeres.append(entry['conta2'])
                      no_espe.append(entry['conta3'])
              if (cat == "Tipo de Establecimiento"):
                  results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                      tipo1=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),
                      tipo2=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                      tipo3=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(muni_id_ente__muni_nombre=municipio, id_institucion__ins_nombre=inst).order_by(
                      'id_institucion__ins_nombre', 'anio')
                  for entry in results:
                      label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                          'anio'])  # guarda nombre del departamento ya agrupado
                      oficial_r.append(entry['tipo1'])
                      no_oficial.append(entry['tipo2'])
                      oficial_u.append(entry['tipo3'])
              if(cat == "Zona"):
                      results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          zona1=Count('id_institucion', filter=Q(id_institucion__ins_zona="RURAL")),
                          zona2=Count('id_institucion', filter=Q(id_institucion__ins_zona="URBANO"))).filter(muni_id_ente__muni_nombre=municipio, id_institucion__ins_nombre=inst).order_by(
                          'id_institucion__ins_nombre', 'anio')
                      for entry in results:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                              'anio'])  # guarda nombre del departamento ya agrupado
                          rural.append(entry['zona1'])
                          urbano.append(entry['zona2'])
              if(cat == "Discapacidad Cognitiva"):
                      results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          con=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),
                          sin=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(muni_id_ente__muni_nombre=municipio, id_institucion__ins_nombre=inst).order_by(
                          'id_institucion__ins_nombre', 'anio')
                      for entry in results:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                              'anio'])  # guarda nombre del departamento ya agrupado
                          con_discapacidad.append(entry['con'])
                          sin_discapacidad.append(entry['sin'])
              else:
                  if(cat == "Nivel de Desempeño Global"):
                      results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                          ins=Count('id_estudiante', filter=Q(desempenio="INSUFICIENTE")),
                          min=Count('id_estudiante', filter=Q(desempenio="MINIMO")),
                          satis=Count('id_estudiante', filter=Q(desempenio="SATISFACTORIO")),
                          avan=Count('id_estudiante', filter=Q(desempenio="AVANZADO"))).filter(muni_id_ente__muni_nombre=municipio, id_institucion__ins_nombre=inst).order_by(
                          'id_institucion__ins_nombre', 'anio')
                      for entry in results:
                          label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                              'anio'])  # guarda nombre del departamento ya agrupado
                          insuficiente.append(entry['ins'])
                          minimo.append(entry['min'])
                          satisfactorio.append(entry['satis'])
                          avanzado.append(entry['avan'])
    else:
        if (municipio == "TODOS"):
            if (cat == "Genero"):
                result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                    conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),# filter=Q es un if dentro de la consulta
                    conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                    conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA"))).filter(anio=anios).order_by(
                    'muni_id_ente__muni_nombre', 'anio')
                for entry in result1:
                    label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                    hombres.append(entry['conta1'])
                    mujeres.append(entry['conta2'])
                    no_espe.append(entry['conta3'])
            if (cat == "Tipo de Establecimiento"):
                result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                    tipo1=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),# filter=Q es un if dentro de la consulta
                    tipo2=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                    tipo3=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO"))).filter(anio=anios).order_by(
                    'muni_id_ente__muni_nombre', 'anio')
                for entry in result1:
                    label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                    oficial_r.append(entry['tipo1'])
                    no_oficial.append(entry['tipo2'])
                    oficial_u.append(entry['tipo3'])
            if (cat == "Zona"):
                    result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                        zona1=Count('id_institucion', filter=Q(id_institucion__ins_zona="RURAL")),  #filter=Q es un if dentro de la consulta
                        zona2=Count('id_institucion', filter=Q(id_institucion__ins_zona="URBANO"))).filter(anio=anios).order_by(
                        'muni_id_ente__muni_nombre', 'anio')
                    for entry in result1:
                        label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                        rural.append(entry['zona1'])
                        urbano.append(entry['zona2'])
            if (cat == "Discapacidad Cognitiva"):
                result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                    con=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),  #filter=Q es un if dentro de la consulta
                    sin=Count('id_institucion', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD"))).filter(anio=anios).order_by(
                    'muni_id_ente__muni_nombre', 'anio')
                for entry in result1:
                    label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                    con_discapacidad.append(entry['con'])
                    sin_discapacidad.append(entry['sin'])
            else:
                if (cat == "Nivel de Desempeño Global"):
                    result1 = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                        ins=Count('id_estudiante', filter=Q(desempenio="INSUFICIENTE")),  #filter=Q es un if dentro de la consulta
                        min=Count('id_institucion', filter=Q(desempenio="MINIMO")),
                        satis=Count('id_institucion', filter=Q(desempenio="SATISFACTORIO")),
                        avan=Count('id_institucion', filter=Q(desempenio="AVANZADO"))).filter(anio=anios).order_by(
                        'muni_id_ente__muni_nombre', 'anio')
                    for entry in result1:
                        label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])
                        insuficiente.append(entry['ins'])
                        minimo.append(entry['min'])
                        satisfactorio.append(entry['satis'])
                        avanzado.append(entry['avan'])
        else:
            if (inst == "General"):
                result1 = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                    conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                    conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                    conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA")),
                    tipo1=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),
                    tipo2=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                    tipo3=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO")),
                    zona1=Count('id_institucion', filter=Q(id_institucion__ins_zona="RURAL")),
                    zona2=Count('id_institucion', filter=Q(id_institucion__ins_zona="URBANO")),
                    con=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),
                    sin=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD")),
                    ins=Count('id_estudiante', filter=Q(desempenio="INSUFICIENTE")),
                    min=Count('id_estudiante', filter=Q(desempenio="MINIMO")),
                    satis=Count('id_estudiante', filter=Q(desempenio="SATISFACTORIO")),
                    avan=Count('id_estudiante', filter=Q(desempenio="AVANZADO"))).filter(
                    muni_id_ente__muni_nombre=municipio, anio=anios).order_by(
                    'id_institucion__ins_nombre',
                    'anio')  # se filtra por genero y por el nombre del municipio que elija el usuario
                for entry in result1:
                    label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                        'anio'])  # guarda nombre del departamento ya agrupado
                    hombres.append(entry['conta1'])
                    mujeres.append(entry['conta2'])
                    no_espe.append(entry['conta3'])
                    oficial_r.append(entry['tipo1'])
                    no_oficial.append(entry['tipo2'])
                    oficial_u.append(entry['tipo3'])
                    rural.append(entry['zona1'])
                    urbano.append(entry['zona2'])
                    con_discapacidad.append(entry['con'])
                    sin_discapacidad.append(entry['sin'])
                    insuficiente.append(entry['ins'])
                    minimo.append(entry['min'])
                    satisfactorio.append(entry['satis'])
                    avanzado.append(entry['avan'])
            else:
                results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                    conta1=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="MASCULINO")),
                    conta2=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="FEMENINO")),
                    conta3=Count('id_estudiante', filter=Q(id_estudiante__estu_sexo="NO_ESPECIFICA")),
                    tipo1=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_RURAL")),
                    tipo2=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="NO_OFICIAL")),
                    tipo3=Count('id_institucion', filter=Q(id_institucion__ins_tipo_estab="OFICIAL_URBANO")),
                    zona1=Count('id_institucion', filter=Q(id_institucion__ins_zona="RURAL")),
                    zona2=Count('id_institucion', filter=Q(id_institucion__ins_zona="URBANO")),
                    con=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="CON_DISCAPACIDAD")),
                    sin=Count('id_estudiante', filter=Q(id_estudiante__estu_dissenso="SIN_DISCAPACIDAD")),
                    ins=Count('id_estudiante', filter=Q(desempenio="INSUFICIENTE")),
                    min=Count('id_estudiante', filter=Q(desempenio="MINIMO")),
                    satis=Count('id_estudiante', filter=Q(desempenio="SATISFACTORIO")),
                    avan=Count('id_estudiante', filter=Q(desempenio="AVANZADO"))).filter(
                    muni_id_ente__muni_nombre=municipio, id_institucion__ins_nombre=inst, anio=anios).order_by(
                    'id_institucion__ins_nombre', 'anio')
                for entry in results:
                    label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                        'anio'])  # guarda nombre del departamento ya agrupado
                    hombres.append(entry['conta1'])
                    mujeres.append(entry['conta2'])
                    no_espe.append(entry['conta3'])
                    oficial_r.append(entry['tipo1'])
                    no_oficial.append(entry['tipo2'])
                    oficial_u.append(entry['tipo3'])
                    rural.append(entry['zona1'])
                    urbano.append(entry['zona2'])
                    con_discapacidad.append(entry['con'])
                    sin_discapacidad.append(entry['sin'])
                    insuficiente.append(entry['ins'])
                    minimo.append(entry['min'])
                    satisfactorio.append(entry['satis'])
                    avanzado.append(entry['avan'])

    return JsonResponse(data={

        'labels': label,
        'data': dato,
        'hombres': hombres,
        'mujeres': mujeres,
        'no_espe': no_espe,
        'oficial_r': oficial_r,
        'no_oficial': no_oficial,
        'oficial_u': oficial_u,
        'rural': rural,
        'urbano': urbano,
        'con_discapacidad': con_discapacidad,
        'sin_discapacidad': sin_discapacidad,
        'insuficiente': insuficiente,
        'minimo': minimo,
        'satisfactorio': satisfactorio,
        'avanzado': avanzado,
    })

def DashboardHtml(request):
    title = "Consulta Dashboard >> Numero de Estudiantes"
    list_url = reverse_lazy('appdash:dashboard_list1')
    entity = 'Consulta'

    contexto = {"title": title, "list_url": list_url, "entity": entity}

    return render(request, "dashboard2/list.html", contexto)