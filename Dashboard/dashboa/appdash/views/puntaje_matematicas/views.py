from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from django.db.models import Count, Avg, Sum, Q
from django.db.models import Func
from appdash.forms import EstudianteForm
#from Dashboard.dashboa.appdash.forms import EstudianteForm

class Round(Func):
    function = 'Round'
    arity = 2

def tabla_puntaje_mat(request):
    title = "Consulta Puntaje Promedio Area de Matematicas por Municipio y Años General"
    list_url = reverse_lazy('appdash:tabla_puntaje_mat_list')
    entity = 'Consulta'

    from django.db import connection
    with connection.cursor() as cursor:
        #cursor.execute("SELECT muni_nombre, ROUND(AVG(puntaje_lenguaje),2) AS PROMEDIO FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) GROUP BY 1 ORDER BY 1")
        cursor.execute("SELECT muni_nombre, anio, ROUND(AVG(puntaje_matematicas),2) AS PROMEDIO FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) GROUP BY anio,muni_nombre ORDER BY anio,muni_nombre")
        results = cursor.fetchall()
        contexto = {"consultas": results, "title": title, "list_url": list_url, "entity": entity}
    return render(request, "puntaje_matematicas/list.html", contexto)

def grafica_puntaje_mat(request):
    muniNombre = []
    prom = []

    try:
        result = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
            prom1=Round(Avg('puntaje_matematicas'), 2)).order_by('muni_id_ente__muni_nombre', 'anio')
        for i in result:
            muniNombre.append(i['muni_id_ente__muni_nombre'] + " : " + i['anio'])
            prom.append(i['prom1'])
    except Exception as e:
        muniNombre['error'] = str(e)
        prom['error'] = str(e)

    return JsonResponse(data={
        'xmuni': muniNombre,
        'ypromedio': prom,

    })



