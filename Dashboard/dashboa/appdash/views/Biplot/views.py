from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5, DimPrueba, DimTiempo
from django.db.models import Sum, Count, Q, Avg
from django.db.models import Func, Count, Min, Max


class Round(Func):
    function = 'Round'
    arity = 2

def tabla_biplot(request):
    title = "Bienvenidos Analisis de Resultados  BIPLOTS"
    title1 = "Correlación de Puntajes de las Diferentes Materias (Varibilidad 65%) "
    title2 = "Correlación de Puntajes de Materias con Respecto a MASCULINO, FEMENINO, NO_ESPECIFICA (Variabilidad 53%)"
    title3 = "Correlación de Puntajes de Materias con Respecto a con ó sín DISCAPACIDAD (Variabilidad 65%)"
    title4 = "Correlación de Puntajes de Materias con Respecto a zona_Rural, Urbano (Variabilidad 45%)"
    title5 = "Correlación Puntajes de las Diferentes Materias con Respecto a Años (Variabilidad 54%)"

    list_url = reverse_lazy('appdash:biplot_list')
    entity = 'Consulta'

    contexto = {
        "title": title,
        "title1": title1,
        "title2": title2,
        "title3": title3,
        "title4": title4,
        "title5": title5,
        "list_url": list_url,
        "entity": entity
    }
    return render(request, 'Biplot/list.html', contexto)

# en la funcion siguiente no se coloco nada porque la vista que esta manejando la grafica que se muestra en la platilla html esta en la vista Biplot/views.py
def grafica_biplot(request):

    return JsonResponse(data={

    })