from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5, DimPrueba, DimTiempo
from django.db.models import Sum, Count, Q, Avg
from django.db.models import Func, Count, Min, Max


import pandas as pd
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import plotly
import numpy as np
import pymysql
from sqlalchemy import create_engine
from django.db import connection    #permite traer la conexion con la BD en postgres y hacer las consultas


class Round(Func):
    function = 'Round'
    arity = 2




def tabla_boxplot(request):
    title = "Bienvenidos Analisis de Resultados Estadisticos BOXPLOT"
    title1 = "Puntaje de Estudiantes"

    list_url = reverse_lazy('appdash:boxplot_list')
    entity = 'Consulta'

    contexto = {
        "title": title,
        "title1": title1,
        "list_url": list_url,
        "entity": entity
    }
    return render(request, 'boxplot/list.html', contexto)


def grafica_boxplot(request):
    Aniosdata = []

    total_leng = []
    total_mat = []
    total_cienc = []
    total_compe = []
    anios = request.GET['anios']


    try:

        result1 = FactSaber5.objects.values('anio','puntaje_lenguaje','puntaje_matematicas','puntaje_ciencias','puntaje_competencias').annotate( #es importante enviar los puntajes en esta linea para formar la grafica completa con media quartiles y bigotes, etc porque sino solo mostraria un solo valor(promedio) en la grafica boxplot
                p_lenguaje=Round(Avg('puntaje_lenguaje'), 2),  #esta linea permite sacar valores con solo dos decimales en el boxplot
                p_matematicas=Round(Avg('puntaje_matematicas'), 2),
                p_ciencias=Round(Avg('puntaje_ciencias'), 2),
                p_competencias=Round(Avg('puntaje_competencias'), 2),
        ).filter(anio=anios).order_by('anio')

        for i in result1:
            Aniosdata.append(i['anio'])
            total_leng.append(i['p_lenguaje'])
            total_mat.append(i['p_matematicas'])
            total_cienc.append(i['p_ciencias'])
            total_compe.append(i['p_competencias'])



    except Exception as e:
        Aniosdata['error'] = str(e)
        total_leng['error'] = str(e)
        total_mat['error'] = str(e)
        total_cienc['error'] = str(e)
        total_compe['error'] = str(e)


    return JsonResponse(data={
        'xAnios': Aniosdata,
        'total_yPuntLengEstu': total_leng,
        'total_yPuntMatEstu': total_mat,
        'total_yPuntCiencEstu': total_cienc,
        'total_yPuntCompeEstu': total_compe,

    })


''' datos = pd.read_csv("appdash/views/biplot_archivo/Ecaes.csv", sep="|", encoding="utf-8")
datos.head(5)

datos.FAMI_ESTRATOVIVIENDA=datos.FAMI_ESTRATOVIVIENDA.astype(str)
datos.dtypes
datos.corr()

def escala_zscore(X,clase):
    df=pd.DataFrame()
    for c in X.columns:
        if X[c].dtype!='object' and c!=clase:
            df[c]=(X[c]-X[c].mean())/(X[c].std())
        else :
            df[c]=(X[c])
    return df


def myplot2(pca,datos,cpx,cpy):
    components =pca.transform(datos)
    coeff=np.transpose(pca.components_[[cpx-1,cpy-1], :])
    fig, ax = plt.subplots(figsize = (14,8))
    xs = components[:,cpx-1]
    ys = components[:,cpy-1]
    n = coeff.shape[0]
    scalex = 1.0/(xs.max() - xs.min())
    scaley = 1.0/(ys.max() - ys.min())
    plt.scatter(xs * scalex,ys * scaley,s=5)
    for i in range(n):
        plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], alpha=0.5,
                  head_width=0.02, head_length=0.02, linewidth=1, color='red')
        plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, datos.columns[i], color='green', ha='center', va='center')
    plt.xlabel("PC{}".format(cpx))
    plt.ylabel("PC{}".format(cpy))
    plt.grid()
    plt.savefig("appdash/static/Boxplot/js/Plot generated using Matplotlib Nueva.png")
    #plt.show()


datos.groupby('FAMI_ESTRATOVIVIENDA').count()['ESTU_CONSECUTIVO']
datos.groupby('FAMI_CABEZAFAMILIA').count()['ESTU_CONSECUTIVO']

columnas=list(datos.corr().columns)
columnas.append("FAMI_ESTRATOVIVIENDA")
columnas.append("FAMI_CABEZAFAMILIA")
columnas

datos[columnas]

datos_df=pd.get_dummies(datos[columnas])
datos_df

X_escala=escala_zscore(datos_df,'clase')
X_escala.head()
datos=X_escala
datos

pca = PCA(n_components=len(datos.corr())).fit(datos)

i=1
acum=0
for x in pca.explained_variance_:
    acum = acum + x / sum(pca.explained_variance_)
    print('Autovalor (', i, '):', x / sum(pca.explained_variance_), 'Acumulada:', acum, "valor:", x)
    i = i + 1

cargas=pd.DataFrame()
cargas['variables']=datos.corr().columns
i=1
for x in pca.explained_variance_:
    cargas['CP' + str(i)] = pca.components_[i - 1, :]
    i = i + 1
cargas

cpx=1
cpy=2
myplot2(pca,datos[datos.columns],cpx,cpy)   '''


# Biplot con la BD de las pruebas saber 5

#datos = open(str('appdash/views/biplot_archivo/ValoresPlausibles_Grado5_2014'), 'a+')
#datos.head(5)

''' data = [datos[datos['ESTU_GENERO']=='F'].PUNT_GLOBAL, datos[datos['ESTU_GENERO']=='M'].PUNT_GLOBAL]
fig7, ax7 = plt.subplots()
ax7.set_title('boxplot comparativo PUNT_GLOBAL')
ax7.boxplot(data)
plt.savefig("appdash/static/inicio/js/Plot generated using Matplotlib Nueva.png")
#plt.show()  '''

#   Biplot Trabajado con consulta sql y libreria pandas igualmente con libreria pymysql y sqlalchemy

''' sql = 'select estu_consecutivo,estu_sexo,ins_nombre,estu_dissenso,estu_estrato,ins_zona,ins_sector,ins_tipo_estab,ins_nivel_socio,muni_nombre,puntaje_lenguaje,puntaje_matematicas,puntaje_ciencias,puntaje_competencias,anio from dim_estudiante,fact_saber5,dim_instituciones,dim_municipios where dim_estudiante.id_estudiante=fact_saber5.id_estudiante and fact_saber5.id_institucion=dim_instituciones.id_institucion and dim_municipios.muni_id_ente=fact_saber5.muni_id_ente'
datos = pd.read_sql_query(sql, con=connection)  #connection es la conexion que se realizo con la BD postgres en el inicio esta la importacion de la conexion

datos.estu_estrato = datos.estu_estrato.astype(str)
datos.dtypes
datos.corr()

def escala_zscore(X,clase):
    df=pd.DataFrame()
    for c in X.columns:
        if X[c].dtype!='object' and c!=clase:
            df[c]=(X[c]-X[c].mean())/(X[c].std())
        else :
            df[c]=(X[c])
    return df

def myplot2(pca,datos,cpx,cpy):
    components =pca.transform(datos)
    coeff=np.transpose(pca.components_[[cpx-1,cpy-1], :])
    fig, ax = plt.subplots(figsize = (14,8))
    xs = components[:,cpx-1]
    ys = components[:,cpy-1]
    n = coeff.shape[0]
    scalex = 1.0/(xs.max() - xs.min())
    scaley = 1.0/(ys.max() - ys.min())
    plt.scatter(xs * scalex,ys * scaley,s=5)
    for i in range(n):
        plt.arrow(0, 0, coeff[i,0], coeff[i,1],alpha = 0.5,
                  head_width=0.02, head_length=0.02, linewidth=1, color='red')
        plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, datos.columns[i], color='green', ha='center', va='center')
    plt.xlabel("PC{}".format(cpx))
    plt.ylabel("PC{}".format(cpy))
    plt.grid()
    plt.savefig("appdash/static/Boxplot/js/Plot generated using Matplotlib Nueva.png")
    #plt.show()


datos.groupby('ins_zona').count()['estu_consecutivo']
datos.groupby('estu_dissenso').count()['estu_consecutivo']
datos.groupby('ins_sector').count()['estu_consecutivo']
datos.groupby('ins_tipo_estab').count()['estu_consecutivo']
datos.groupby('ins_nivel_socio').count()['estu_consecutivo']
datos.groupby('anio').count()['estu_consecutivo']

columnas=list(datos.corr().columns)
columnas.append("ins_zona")
columnas.append("estu_dissenso")
columnas.append("ins_sector")
columnas.append("ins_tipo_estab")
columnas.append("ins_nivel_socio")
columnas
datos[columnas]
datos_df=pd.get_dummies(datos[columnas])
datos_df

X_escala=escala_zscore(datos_df,'clase')
X_escala.head()

datos=X_escala
datos

pca = PCA(n_components=len(datos.corr())).fit(datos)
i=1
acum=0
for x in pca.explained_variance_:
    acum=acum+x/sum(pca.explained_variance_)
    print('Autovalor (',i,'):', x/sum(pca.explained_variance_),'Acumulada:',acum,"valor:",x)
    i=i+1

cargas=pd.DataFrame()
cargas['variables']=datos.corr().columns
i=1
for x in pca.explained_variance_:
    cargas['CP'+str(i)]=pca.components_[i-1,:]
    i=i+1
cargas

cpx=18
cpy=1
myplot2(pca,datos[datos.columns],cpx,cpy)     '''

#   Biplot 1 Trabajado con consulta sql y libreria pandas igualmente con libreria pymysql y sqlalchemy
#   Grafica las correlaciones de los puntajes segun la carga que se generan en los componentes EJM: los estudiantes que estan
#   en la parte positiva son a los que mejor les fue en ciencias

sql = 'select estu_consecutivo,estu_sexo,ins_nombre,estu_dissenso,estu_estrato,ins_zona,ins_sector,ins_tipo_estab,ins_nivel_socio,muni_nombre,puntaje_lenguaje,puntaje_matematicas,puntaje_ciencias,puntaje_competencias,anio from dim_estudiante,fact_saber5,dim_instituciones,dim_municipios where dim_estudiante.id_estudiante=fact_saber5.id_estudiante and fact_saber5.id_institucion=dim_instituciones.id_institucion and dim_municipios.muni_id_ente=fact_saber5.muni_id_ente'
datos = pd.read_sql_query(sql,
                          con=connection)  # connection es la conexion que se realizo con la BD postgres en el inicio esta la importacion de la conexion

datos.corr()


def escala_zscore(X, clase):
    df = pd.DataFrame()
    for c in X.columns:
        if X[c].dtype != 'object' and c != clase:
            df[c] = (X[c] - X[c].mean()) / (X[c].std())
        else:
            df[c] = (X[c])
    return df

var=['puntaje_ciencias','puntaje_lenguaje','puntaje_matematicas','puntaje_competencias']
datos_dm=pd.get_dummies(datos[var])
datos_dm.head()


from sklearn.decomposition import PCA

var = ['puntaje_ciencias', 'puntaje_lenguaje', 'puntaje_matematicas', 'puntaje_competencias']
pca = PCA(n_components=4).fit(datos[var])

i = 1
acum = 0
for x in pca.explained_variance_:
    acum = acum + x / sum(pca.explained_variance_)
    print('Autovalor (', i, '):', x / sum(pca.explained_variance_), 'Acumulada:', acum)
    i = i + 1

cargas = pd.DataFrame()
cargas['variables'] = var
i = 1
for x in pca.explained_variance_:
    cargas['CP' + str(i)] = pca.components_[i - 1, :]
    i = i + 1
cargas

import numpy as np

print("Varianza total Traza:", np.trace(datos_dm[var].cov()))
print("Varianza total suma autovalores:", sum(pca.explained_variance_))


def myplot2(pca, datos, cpx, cpy):
    components = pca.fit_transform(datos)
    coeff = np.transpose(pca.components_[[cpx - 1, cpy - 1], :])
    fig, ax = plt.subplots(figsize=(14, 8))
    xs = components[:, cpx - 1]
    ys = components[:, cpy - 1]
    n = coeff.shape[0]
    scalex = 1.0 / (xs.max() - xs.min())
    scaley = 1.0 / (ys.max() - ys.min())
    plt.scatter(xs * scalex, ys * scaley, s=5)
    for i in range(n):
        plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], alpha=0.5,
                  head_width=0.02, head_length=0.02, linewidth=1, color='red')

        plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, datos.columns[i], color='green', ha='center', va='center')

    plt.xlabel("PC{}".format(cpx))
    plt.ylabel("PC{}".format(cpy))

    plt.grid()
    plt.savefig("appdash/static/Boxplot/js/Plot generated using Matplotlib Dos.png")
    #plt.show()


myplot2(pca, datos[var], 1, 2)

# ---------------------------------------        Biplot 2     ------------------------------------------------------------------------

sql = 'select estu_consecutivo,estu_sexo,ins_nombre,estu_dissenso,estu_estrato,ins_zona,ins_sector,ins_tipo_estab,ins_nivel_socio,muni_nombre,puntaje_lenguaje,puntaje_matematicas,puntaje_ciencias,puntaje_competencias,anio from dim_estudiante,fact_saber5,dim_instituciones,dim_municipios where dim_estudiante.id_estudiante=fact_saber5.id_estudiante and fact_saber5.id_institucion=dim_instituciones.id_institucion and dim_municipios.muni_id_ente=fact_saber5.muni_id_ente'
datos = pd.read_sql_query(sql, con=connection)  # connection es la conexion que se realizo con la BD postgres en el inicio esta la importacion de la conexion

datos.groupby('estu_sexo').count()['estu_consecutivo']


def escala_zscore(X, clase):
    df = pd.DataFrame()
    for c in X.columns:
        if X[c].dtype != 'object' and c != clase:
            df[c] = (X[c] - X[c].mean()) / (X[c].std())
        else:
            df[c] = (X[c])
    return df


var = ['puntaje_ciencias', 'puntaje_lenguaje', 'puntaje_matematicas', 'puntaje_competencias', 'estu_sexo']
datos_dm = pd.get_dummies(datos[var])
datos_dm.head()

from sklearn.decomposition import PCA

var = ['puntaje_ciencias', 'puntaje_lenguaje', 'puntaje_matematicas', 'puntaje_competencias',
       'estu_sexo_FEMENINO', 'estu_sexo_MASCULINO', 'estu_sexo_NO_ESPECIFICA']
pca = PCA(n_components=7).fit(datos_dm[var])

i = 1
acum = 0
for x in pca.explained_variance_:
    acum = acum + x / sum(pca.explained_variance_)
    print('Autovalor (', i, '):', x / sum(pca.explained_variance_), 'Acumulada:', acum)
    i = i + 1

cargas = pd.DataFrame()
cargas['variables'] = var
i = 1
for x in pca.explained_variance_:
    cargas['CP' + str(i)] = pca.components_[i - 1, :]
    i = i + 1
cargas

import numpy as np

print("Varianza total Traza:", np.trace(datos_dm[var].cov()))
print("Varianza total suma autovalores:", sum(pca.explained_variance_))


def myplot2(pca, datos, cpx, cpy):
    components = pca.fit_transform(datos)
    coeff = np.transpose(pca.components_[[cpx - 1, cpy - 1], :])
    fig, ax = plt.subplots(figsize=(14, 8))
    xs = components[:, cpx - 1]
    ys = components[:, cpy - 1]
    n = coeff.shape[0]
    scalex = 1.0 / (xs.max() - xs.min())
    scaley = 1.0 / (ys.max() - ys.min())
    plt.scatter(xs * scalex, ys * scaley, s=5)
    for i in range(n):
        plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], alpha=0.5,
                  head_width=0.02, head_length=0.02, linewidth=1, color='red')

        plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, datos.columns[i], color='green', ha='center', va='center')

    plt.xlabel("PC{}".format(cpx))
    plt.ylabel("PC{}".format(cpy))

    plt.grid()
    plt.savefig("appdash/static/Boxplot/js/Plot generated using Matplotlib Nueva 3.png")
    #plt.show()


myplot2(pca, datos_dm[var], 1, 2)

#_______________________________________________ Biplot Grafica 3________________________________________________

sql = 'select estu_consecutivo,estu_sexo,ins_nombre,estu_dissenso,estu_estrato,ins_zona,ins_sector,ins_tipo_estab,ins_nivel_socio,muni_nombre,puntaje_lenguaje,puntaje_matematicas,puntaje_ciencias,puntaje_competencias,anio from dim_estudiante,fact_saber5,dim_instituciones,dim_municipios where dim_estudiante.id_estudiante=fact_saber5.id_estudiante and fact_saber5.id_institucion=dim_instituciones.id_institucion and dim_municipios.muni_id_ente=fact_saber5.muni_id_ente'
datos = pd.read_sql_query(sql,
                          con=connection)  # connection es la conexion que se realizo con la BD postgres en el inicio esta la importacion de la conexion

datos.groupby('estu_dissenso').count()['estu_consecutivo']


def escala_zscore(X, clase):
    df = pd.DataFrame()
    for c in X.columns:
        if X[c].dtype != 'object' and c != clase:
            df[c] = (X[c] - X[c].mean()) / (X[c].std())
        else:
            df[c] = (X[c])
    return df


var = ['puntaje_ciencias', 'puntaje_lenguaje', 'puntaje_matematicas', 'puntaje_competencias', 'estu_dissenso']
datos_dm = pd.get_dummies(datos[var])
datos_dm.head()

from sklearn.decomposition import PCA

var = ['puntaje_ciencias', 'puntaje_lenguaje', 'puntaje_matematicas', 'puntaje_competencias',
       'estu_dissenso_CON_DISCAPACIDAD', 'estu_dissenso_SIN_DISCAPACIDAD']
pca = PCA(n_components=6).fit(datos_dm[var])

i = 1
acum = 0
for x in pca.explained_variance_:
    acum = acum + x / sum(pca.explained_variance_)
    print('Autovalor (', i, '):', x / sum(pca.explained_variance_), 'Acumulada:', acum)
    i = i + 1

cargas = pd.DataFrame()
cargas['variables'] = var
i = 1
for x in pca.explained_variance_:
    cargas['CP' + str(i)] = pca.components_[i - 1, :]
    i = i + 1
cargas

import numpy as np

print("Varianza total Traza:", np.trace(datos_dm[var].cov()))
print("Varianza total suma autovalores:", sum(pca.explained_variance_))


def myplot2(pca, datos, cpx, cpy):
    components = pca.fit_transform(datos)
    coeff = np.transpose(pca.components_[[cpx - 1, cpy - 1], :])
    fig, ax = plt.subplots(figsize=(14, 8))
    xs = components[:, cpx - 1]
    ys = components[:, cpy - 1]
    n = coeff.shape[0]
    scalex = 1.0 / (xs.max() - xs.min())
    scaley = 1.0 / (ys.max() - ys.min())
    plt.scatter(xs * scalex, ys * scaley, s=5)
    for i in range(n):
        plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], alpha=0.5,
                  head_width=0.02, head_length=0.02, linewidth=1, color='red')

        plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, datos.columns[i], color='green', ha='center', va='center')

    plt.xlabel("PC{}".format(cpx))
    plt.ylabel("PC{}".format(cpy))

    plt.grid()
    plt.savefig("appdash/static/Boxplot/js/Plot generated using Matplotlib Nueva 4.png")
    #plt.show()


myplot2(pca, datos_dm[var], 1, 2)



# ______ Biplot Grafica 4 tienen un porcentaje de variablilidad del 45 % se grafico pc1 con respecto a pc3___________________________



sql = 'select estu_consecutivo,estu_sexo,ins_nombre,estu_dissenso,estu_estrato,ins_zona,ins_sector,ins_tipo_estab,ins_nivel_socio,muni_nombre,puntaje_lenguaje,puntaje_matematicas,puntaje_ciencias,puntaje_competencias,anio from dim_estudiante,fact_saber5,dim_instituciones,dim_municipios where dim_estudiante.id_estudiante=fact_saber5.id_estudiante and fact_saber5.id_institucion=dim_instituciones.id_institucion and dim_municipios.muni_id_ente=fact_saber5.muni_id_ente'
datos = pd.read_sql_query(sql, con=connection)  # connection es la conexion que se realizo con la BD postgres en el inicio esta la importacion de la conexion

datos.groupby('ins_zona').count()['estu_consecutivo']


def escala_zscore(X, clase):
    df = pd.DataFrame()
    for c in X.columns:
        if X[c].dtype != 'object' and c != clase:
            df[c] = (X[c] - X[c].mean()) / (X[c].std())
        else:
            df[c] = (X[c])
    return df


var = ['puntaje_ciencias', 'puntaje_lenguaje', 'puntaje_matematicas', 'puntaje_competencias', 'ins_zona']
datos_dm = pd.get_dummies(datos[var])
datos_dm.head()

from sklearn.decomposition import PCA

var = ['puntaje_ciencias', 'puntaje_lenguaje', 'puntaje_matematicas', 'puntaje_competencias',
       'ins_zona_RURAL', 'ins_zona_URBANO']
pca = PCA(n_components=6).fit(datos_dm[var])

i = 1
acum = 0
for x in pca.explained_variance_:
    acum = acum + x / sum(pca.explained_variance_)
    print('Autovalor (', i, '):', x / sum(pca.explained_variance_), 'Acumulada:', acum)
    i = i + 1

cargas = pd.DataFrame()
cargas['variables'] = var
i = 1
for x in pca.explained_variance_:
    cargas['CP' + str(i)] = pca.components_[i - 1, :]
    i = i + 1
cargas

import numpy as np

print("Varianza total Traza:", np.trace(datos_dm[var].cov()))
print("Varianza total suma autovalores:", sum(pca.explained_variance_))


def myplot2(pca, datos, cpx, cpy):
    components = pca.fit_transform(datos)
    coeff = np.transpose(pca.components_[[cpx - 1, cpy - 1], :])
    fig, ax = plt.subplots(figsize=(14, 8))
    xs = components[:, cpx - 1]
    ys = components[:, cpy - 1]
    n = coeff.shape[0]
    scalex = 1.0 / (xs.max() - xs.min())
    scaley = 1.0 / (ys.max() - ys.min())
    plt.scatter(xs * scalex, ys * scaley, s=5)
    for i in range(n):
        plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], alpha=0.5,
                  head_width=0.02, head_length=0.02, linewidth=1, color='red')

        plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, datos.columns[i], color='green', ha='center', va='center')

    plt.xlabel("PC{}".format(cpx))
    plt.ylabel("PC{}".format(cpy))

    plt.grid()
    plt.savefig("appdash/static/Boxplot/js/Plot generated using Matplotlib Nueva 5.png")
    #plt.show()


myplot2(pca, datos_dm[var], 1, 3)

#   Biplot 5    *************** Grafica las correlaciones de los puntajes con respecto a los años   **********************************


sql = 'select estu_consecutivo,estu_sexo,ins_nombre,estu_dissenso,estu_estrato,ins_zona,ins_sector,ins_tipo_estab,ins_nivel_socio,muni_nombre,puntaje_lenguaje,puntaje_matematicas,puntaje_ciencias,puntaje_competencias,anio from dim_estudiante,fact_saber5,dim_instituciones,dim_municipios where dim_estudiante.id_estudiante=fact_saber5.id_estudiante and fact_saber5.id_institucion=dim_instituciones.id_institucion and dim_municipios.muni_id_ente=fact_saber5.muni_id_ente'
datos = pd.read_sql_query(sql, con=connection)  #connection es la conexion que se realizo con la BD postgres en el inicio esta la importacion de la conexion


datos.groupby('ins_zona').count()['estu_consecutivo']
datos['anio']=datos.anio.astype(str)

def escala_zscore(X,clase):
    df=pd.DataFrame()
    for c in X.columns:
        if X[c].dtype!='object' and c!=clase:
            df[c]=(X[c]-X[c].mean())/(X[c].std())
        else :
            df[c]=(X[c])
    return df

var=['puntaje_ciencias','puntaje_lenguaje','puntaje_matematicas','puntaje_competencias','anio']
datos_dm=pd.get_dummies(datos[var])
datos_dm.head()

from sklearn.decomposition import PCA
var=['puntaje_ciencias','puntaje_lenguaje','puntaje_matematicas','puntaje_competencias',
    'anio_2014','anio_2015','anio_2016','anio_2017']
pca = PCA(n_components=8).fit(datos_dm[var])

i=1
acum=0
for x in pca.explained_variance_:
    acum=acum+x/sum(pca.explained_variance_)
    print('Autovalor (',i,'):', x/sum(pca.explained_variance_),'Acumulada:',acum)
    i=i+1

cargas=pd.DataFrame()
cargas['variables']=var
i=1
for x in pca.explained_variance_:
    cargas['CP'+str(i)]=pca.components_[i-1,:]
    i=i+1
cargas

print("Varianza total Traza:", np.trace(datos_dm[var].cov()))
print("Varianza total suma autovalores:",sum(pca.explained_variance_))


def myplot2(pca, datos, cpx, cpy):
    components = pca.fit_transform(datos)
    coeff = np.transpose(pca.components_[[cpx - 1, cpy - 1], :])
    fig, ax = plt.subplots(figsize=(14, 8))
    xs = components[:, cpx - 1]
    ys = components[:, cpy - 1]
    n = coeff.shape[0]
    scalex = 1.0 / (xs.max() - xs.min())
    scaley = 1.0 / (ys.max() - ys.min())
    plt.scatter(xs * scalex, ys * scaley, s=5)
    for i in range(n):
        plt.arrow(0, 0, coeff[i, 0], coeff[i, 1], alpha=0.5,
                  head_width=0.02, head_length=0.02, linewidth=1, color='red')

        plt.text(coeff[i, 0] * 1.15, coeff[i, 1] * 1.15, datos.columns[i], color='green', ha='center', va='center')

    plt.xlabel("PC{}".format(cpx))
    plt.ylabel("PC{}".format(cpy))

    plt.grid()
    plt.savefig("appdash/static/Boxplot/js/Plot generated using Matplotlib Nueva.png")
    #plt.show()


myplot2(pca, datos_dm[var], 1, 2)


