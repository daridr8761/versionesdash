from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from django.db.models import Count, Avg, Sum
from appdash.forms import EstudianteForm
#from Dashboard.dashboa.appdash.forms import EstudianteForm


def grafichtml(request):
    title = "Consulta Numero de Estudiantes por Municipio General"
    list_url = reverse_lazy('appdash:graficaest_list')
    entity = 'Consulta'

    from django.db import connection
    with connection.cursor() as cursor:
        cursor.execute("SELECT muni_nombre,COUNT(id_estudiante) as suma FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) JOIN dim_estudiante USING(id_estudiante) GROUP BY 1 ORDER BY 1")
        # cursor.execute("SELECT muni_nombre, ROUND(AVG(puntaje_lenguaje),2) AS PROMEDIO FROM dim_municipios JOIN fact_saber5 USING(muni_id_ente) GROUP BY 1 ORDER BY 1")
        results = cursor.fetchall()

        # rawData = list(cursor.fetchall())
        # rawData = namedtuplefetchall(cursor)
        # print(rawData[0][1])
        # result = []
        # for r in rawData:
        #   result.append(list(r))
        contexto = {"consultas": results, "title": title, "list_url": list_url, "entity": entity}
    return render(request, "graficaest/list.html", contexto)

def grafica(request):
    data = []
    munidata = []
    try:
        for i in FactSaber5.objects.values('muni_id_ente__muni_nombre').annotate(conta=Count('id_estudiante')):
            data.append(i['conta'])
            munidata.append(i['muni_id_ente__muni_nombre'])
    except Exception as e:
        data['error'] = str(e)
    return JsonResponse(data={
        'ynum': data,
        'xmuni': munidata,
    })



