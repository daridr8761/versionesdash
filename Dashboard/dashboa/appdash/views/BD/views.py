import codecs
import pandas as pd
import psycopg2
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import ArchivoCarga
from django.contrib import messages
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#from Dashboard.dashboa.appdash.models import ArchivoCarga

@csrf_exempt

def BdListView(request):
    title = "Carga Nuevo Año a las Dimensiones Pruebas Saber 5"
    list_url = reverse_lazy('appdash:BD_list')
    entity = 'Carga de Datos'

    if request.method=='POST':
        traer = ArchivoCarga()
        traer.datos=request.FILES.get('boton_bd')  #trae archivo seleccion usuario del html
        deli = request.POST.get('boton')  #delimitador escogido por usuario
        traer.save()

        fichero = codecs.open(str(traer.datos),encoding='utf-8', )  #trae archivo .txt
        fichero2= open(str('appdash/views/icfes/data.txt'), 'a+') #creacion de archivo para ingresar datos de icfes  a+ = "sino existe el archi lo crea"
        for line in fichero:
            fichero2.write(line.replace(deli, ';'))        # reemplazar delimitador del .txt de las pruebas icfes por ;
        fichero.close()
        fichero2.close()

        fichero2 = open(str('appdash/views/icfes/data.txt'), 'r')  # archivo nuevo para trabajar limpieza
        alerta = fichero2.readline()
        print(alerta)
        fichero2.close()
        mat = pd.read_csv(str(BASE_DIR + '/icfes/data.txt'), encoding='unicode_escape', sep=";", dtype='unicode', error_bad_lines=False) #escoge cabeceras coloca como atributos "aqui nos permite tener los datos en una matriz"
        mat = mat.drop(mat[(mat['DEPARTAMENTO'] != '52')].index)
        mat = mat.drop(mat[(mat['MUNICIPIO'] != '52022') &
                           (mat['MUNICIPIO'] != '52210') &
                           (mat['MUNICIPIO'] != '52215') &
                           (mat['MUNICIPIO'] != '52224') &
                           (mat['MUNICIPIO'] != '52227') &
                           (mat['MUNICIPIO'] != '52287') &
                           (mat['MUNICIPIO'] != '52317') &
                           (mat['MUNICIPIO'] != '52323') &
                           (mat['MUNICIPIO'] != '52352') &
                           (mat['MUNICIPIO'] != '52356') &
                           (mat['MUNICIPIO'] != '52560') &
                           (mat['MUNICIPIO'] != '52573') &
                           (mat['MUNICIPIO'] != '52585')].index)  # borratodo lo que sea diferente de nariño dentro de la matriz
        from sqlalchemy import create_engine
        engine = create_engine('postgresql://dario:Dario3761410@localhost:5432/data_mart_5')   # conexion con base de datos
        mat.to_sql("temporal", engine)   # transforma la matriz del panda y le dara un nombre

        messages.success(request, 'TABLA VALORES PLAUSIBLES Cargada Exitosamente')

        try:
            connect = psycopg2.connect(database='data_mart_5', user='dario', password='Dario3761410', host='localhost', port=5432)
            cursor = connect.cursor(cursor_factory=psycopg2.extras.DictCursor)

            sql = """   update temporal set "GRUPO"='CENSAL'  where "GRUPO"='99';
                        update temporal set "GRUPO"='CONTROL'  where "GRUPO"='01';
                        update temporal set "GRUPO"='CONTROL'  where "GRUPO"='02';
                        
                        update temporal set "DISSENSO"='SIN_DISCAPACIDAD'  where "DISSENSO"='0';
                        update temporal set "DISSENSO"='CON_DISCAPACIDAD'  where "DISSENSO"='1';
                        
                        update temporal set "N"=null where "N"='NO_DEFINIDO';                 
                        
                        update temporal set "SEXO"='MASCULINO' WHERE "SEXO"='1';
                        update temporal set "SEXO"='FEMENINO' WHERE "SEXO"='2';
                        update temporal set "SEXO"='NO_ESPECIFICA' WHERE "SEXO"='3';
                        
                        update temporal set "SECTOR"='OFICIAL' WHERE "SECTOR"='1';
                        update temporal set "SECTOR"='NO_OFICIAL' WHERE "SECTOR"='2';
                        
                        update temporal set "ZONA"='URBANA' WHERE "ZONA"='1';
                        update temporal set "ZONA"='RURAL' WHERE "ZONA"='2';
                        
                        update temporal set "ZONASTAB"='OFICIAL_URBANO'  where"ZONASTAB"='1'; 
                        update temporal set "ZONASTAB"='OFICIAL_RURAL'  where "ZONASTAB"='2'; 
                        update temporal set "ZONASTAB"='NO_OFICIAL'  where "ZONASTAB"='3'; 
                        
                        update temporal set "DISSENSO"='N' WHERE "DISSENSO"='0';
                        update temporal set "DISSENSO"='S' WHERE "DISSENSO"='1';
                        
                        update temporal set "LENG_COPIETAS"=0 WHERE "LENG_COPIETAS" is null;
                        update temporal set "LENG_WEIGHT"=0 WHERE "LENG_WEIGHT" is null;
                        update temporal set "LENG_SCORE1"=0 WHERE "LENG_SCORE1" is null;
                        update temporal set "LENG_SCORE2"=0 WHERE "LENG_SCORE2" is null;
                        update temporal set "LENG_SCORE3"=0 WHERE"LENG_SCORE3" is null;
                        update temporal set "LENG_SCORE4"=0 WHERE "LENG_SCORE4" is null;
                        update temporal set "LENG_SCORE5"=0 WHERE"LENG_SCORE5" is null;
                        
                        update temporal set "MATE_COPIETAS"=0 WHERE "MATE_COPIETAS" is null;
                        update temporal set "MATE_WEIGHT"=0 WHERE "MATE_WEIGHT" is null;
                        update temporal set "MATE_SCORE1"=0 WHERE "MATE_SCORE1" is null;
                        update temporal set "MATE_SCORE2"=0 WHERE "MATE_SCORE2" is null;
                        update temporal set "MATE_SCORE3"=0 WHERE "MATE_SCORE3" is null;
                        update temporal set "MATE_SCORE4"=0 WHERE "MATE_SCORE4" is null;
                        update temporal set "MATE_SCORE5"=0 WHERE "MATE_SCORE5" is null;
                        
                        update temporal set "COMP_COPIETAS"=0 WHERE "COMP_COPIETAS" is null;
                        update temporal set "COMP_WEIGHT"=0 WHERE "COMP_WEIGHT" is null;
                        update temporal set "COMP_SCORE1"=0 WHERE "COMP_SCORE1" is null;
                        update temporal set "COMP_SCORE2"=0 WHERE "COMP_SCORE2" is null;
                        update temporal set "COMP_SCORE3"=0 WHERE "COMP_SCORE3" is null;
                        update temporal set "COMP_SCORE4"=0 WHERE "COMP_SCORE4" is null;
                        update temporal set "COMP_SCORE5"=0 WHERE "COMP_SCORE5" is null;
                        
                        update temporal set "CIEN_COPIETAS"=0 WHERE "CIEN_COPIETAS" is null;
                        update temporal set "CIEN_WEIGHT"=0 WHERE "CIEN_WEIGHT" is null;
                        update temporal set "CIEN_SCORE1"=0 WHERE "CIEN_SCORE1" is null;
                        update temporal set "CIEN_SCORE2"=0 WHERE "CIEN_SCORE2" is null;
                        update temporal set "CIEN_SCORE3"=0 WHERE "CIEN_SCORE3" is null;
                        update temporal set "CIEN_SCORE4"=0 WHERE "CIEN_SCORE4" is null;
                        update temporal set "CIEN_SCORE5"=0 WHERE "CIEN_SCORE5" is null;
                        
                        update temporal set "LENG_COPIETAS"='NO_COPIA' WHERE "LENG_COPIETAS"='0';
                        update temporal set "LENG_COPIETAS"='CON_COPIA' WHERE "LENG_COPIETAS"='1';
                        
                        update temporal set "MATE_COPIETAS"='NO_COPIA' WHERE "MATE_COPIETAS"='0';
                        update temporal set "MATE_COPIETAS"='CON_COPIA' WHERE "MATE_COPIETAS"='1';
                        
                        update temporal set "COMP_COPIETAS"='NO_COPIA' WHERE "COMP_COPIETAS"='0';
                        update temporal set "COMP_COPIETAS"='CON_COPIA' WHERE "COMP_COPIETAS"='1';
                        
                        update temporal set "CIEN_COPIETAS"='NO_COPIA' WHERE "CIEN_COPIETAS"='0';
                        update temporal set "CIEN_COPIETAS"='CON_COPIA' WHERE "CIEN_COPIETAS"='1';
                        
                        update temporal SET "LENG_SCORE1" =upper(translate("LENG_SCORE1"  ,',','.'));
                        update temporal SET "MATE_SCORE1" =upper(translate("MATE_SCORE1"  ,',','.'));
                        update temporal SET "CIEN_SCORE1" =upper(translate("CIEN_SCORE1"  ,',','.'));
                        update temporal SET "COMP_SCORE1" =upper(translate("COMP_SCORE1"  ,',','.'));
                        
                        ALTER TABLE temporal ALTER COLUMN "ESTU_CONSECUTIVO" TYPE varchar  USING (trim("ESTU_CONSECUTIVO")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "GRUPO" TYPE varchar  USING (trim("GRUPO")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "N" TYPE varchar  USING (trim("N")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "ESTRATO" TYPE varchar  USING (trim("ESTRATO")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "COD_DANE" TYPE bigint  USING (trim("COD_DANE")::bigint);
                        ALTER TABLE temporal ALTER COLUMN "GRADO" TYPE varchar  USING (trim("GRADO")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "MUNICIPIO" TYPE integer  USING (trim("MUNICIPIO")::integer);
                        ALTER TABLE temporal ALTER COLUMN "SEXO" TYPE varchar  USING (trim("SEXO")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "SECTOR" TYPE varchar  USING (trim("SECTOR")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "ZONA" TYPE varchar  USING (trim("ZONA")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "ZONASTAB" TYPE varchar  USING (trim("ZONASTAB")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "CALENDARIO" TYPE varchar  USING (trim("CALENDARIO")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "NIVEL" TYPE varchar  USING (trim("NIVEL")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "DISSENSO" TYPE varchar  USING (trim("DISSENSO")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "LENG_COPIETAS" TYPE varchar  USING (trim("LENG_COPIETAS")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "MATE_COPIETAS" TYPE varchar  USING (trim("MATE_COPIETAS")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "CIEN_COPIETAS" TYPE varchar  USING (trim("CIEN_COPIETAS")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "COMP_COPIETAS" TYPE varchar  USING (trim("COMP_COPIETAS")::varchar);
                        ALTER TABLE temporal ALTER COLUMN "LENG_SCORE1" TYPE numeric  USING (trim("LENG_SCORE1")::numeric);
                        ALTER TABLE temporal ALTER COLUMN "MATE_SCORE1" TYPE numeric  USING (trim("MATE_SCORE1")::numeric);
                        ALTER TABLE temporal ALTER COLUMN "CIEN_SCORE1" TYPE numeric  USING (trim("CIEN_SCORE1")::numeric);
                        ALTER TABLE temporal ALTER COLUMN "COMP_SCORE1" TYPE numeric  USING (trim("COMP_SCORE1")::numeric);
                        
                        UPDATE temporal SET "LENG_SCORE1" = ABS("LENG_SCORE1") where "LENG_SCORE1" < 0;
                        UPDATE temporal SET "MATE_SCORE1" = ABS("MATE_SCORE1") where "MATE_SCORE1" < 0;
                        UPDATE temporal SET "CIEN_SCORE1" = ABS("CIEN_SCORE1") where "CIEN_SCORE1" < 0;
                        UPDATE temporal SET "COMP_SCORE1" = ABS("COMP_SCORE1") where "COMP_SCORE1" < 0;
                        
                        
                        ALTER TABLE temporal ADD "año" text;
                        ALTER TABLE temporal ADD "promedio" numeric;
                        ALTER TABLE temporal ADD "desempeño" varchar;
                        
                        update temporal t1 set "año" = (select substr("ESTU_CONSECUTIVO",2,4) from temporal t2 where t1."ESTU_CONSECUTIVO"=t2."ESTU_CONSECUTIVO");
                        UPDATE temporal t1 set "promedio" = (SELECT ROUND(SUM("LENG_SCORE1"+"MATE_SCORE1"+"CIEN_SCORE1"+"COMP_SCORE1")/2,2) from temporal t2 where t1."ESTU_CONSECUTIVO"=t2."ESTU_CONSECUTIVO");
                        UPDATE temporal t1 set "desempeño" = (SELECT ROUND(SUM("LENG_SCORE1"+"MATE_SCORE1"+"CIEN_SCORE1"+"COMP_SCORE1")/2,2) from temporal t2 where t1."ESTU_CONSECUTIVO"=t2."ESTU_CONSECUTIVO");
                        
                        UPDATE temporal SET "desempeño"='INSUFICIENTE' WHERE "desempeño" <='1';
                        UPDATE temporal SET "desempeño"='MINIMO' WHERE "desempeño" >'1' and "desempeño" <='2';
                        UPDATE temporal SET "desempeño"='SATISFACTORIO' WHERE "desempeño" >'2' and "desempeño" <='3';
                        UPDATE temporal SET "desempeño"='AVANZADO' WHERE "desempeño" >'3' and "desempeño" <='4';
                        
                        do $$
                                        declare                
                                        conta integer;
                                        orden_sql text;
                                        orden_sql1 text;
                                orden_sql2 text;                 		
                                        begin
                                        conta:= count(*) from fact_saber5;				
                                        orden_sql := 'create sequence sequen start with '|| conta ||' increment by 1 maxvalue 99999999 minvalue '|| conta ||' cycle';
                                        execute orden_sql;
                                        orden_sql1 := 'create sequence sequen1 start with '|| conta ||' increment by 1 maxvalue 99999999 minvalue '|| conta ||' cycle';
                                        execute orden_sql1; 
                                orden_sql2 := 'create sequence sequen2 start with '|| conta ||' increment by 1 maxvalue 99999999 minvalue '|| conta ||' cycle';
                                        execute orden_sql2;               		
                        end $$;                
                                        alter table fact_saber5 alter column id_estudiante set default nextval('sequen');	                    
                                alter table fact_saber5 alter column id_prueba set default nextval('sequen1'); 
                                alter table fact_saber5 alter column id_tiempo set default nextval('sequen2');
                        
                        insert into dim_estudiante(
                                        estu_consecutivo,
                                estu_grupo,
                                estu_n,
                                estu_estrato,
                                        estu_grado,
                                        estu_sexo,
                                estu_dissenso)
                                        select
                                        "ESTU_CONSECUTIVO",
                                "GRUPO",
                                "N",
                                "ESTRATO",
                                        "GRADO",               
                                        "SEXO",
                                "DISSENSO"
                                        from temporal;
                        
                        
                        
                        insert into dim_prueba(
                                        prue_estu_consecutivo,
                                prue_sn_lenguaje,
                                prue_sn_matematicas,
                                prue_sn_ciencias_naturales,
                                        prue_sn_competencias)
                                        select
                                        "ESTU_CONSECUTIVO",
                                "LENG_COPIETAS",
                                "MATE_COPIETAS",
                                "CIEN_COPIETAS",
                                        "COMP_COPIETAS"                
                                        from temporal;
                        
                        
                        insert into dim_tiempo(
                                        prueba,
                                anio)
                                        select
                                        "ESTU_CONSECUTIVO",
                                "año"                
                                        from temporal;
                        
                        
                        
                        insert into fact_saber5(
                                        muni_id_ente,
                                id_institucion,
                                id_hoja,
                                puntaje_lenguaje,
                                puntaje_matematicas,
                                puntaje_ciencias,
                                        puntaje_competencias,
                                anio,
                                promedio,
                                "desempenio")
                                        select
                                        "MUNICIPIO",
                                "COD_DANE",
                                "ESTU_CONSECUTIVO",
                                "LENG_SCORE1",
                                "MATE_SCORE1",
                                "CIEN_SCORE1",
                                        "COMP_SCORE1",
                                "año",
                                promedio,
                                "desempeño"                
                                        from temporal;
                        
                        drop sequence sequen cascade;
                        drop sequence sequen1 cascade;
                        drop sequence sequen2 cascade;
                        DROP TABLE temporal;
                """

            cursor.execute(sql)
        except (Exception, psycopg2.OperationalError) as error:
            print(error)
        cursor.close()
        connect.commit()
        connect.close()


    contexto = {"title": title, "list_url": list_url, "entity": entity}

    return render(request, 'BD/list.html', contexto)


