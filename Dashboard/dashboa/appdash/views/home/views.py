from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5, DimPrueba, DimTiempo
from appdash.forms import EstudianteForm
from django.db.models import Count, Avg, Sum, Func

class Round(Func):
 function = 'ROUND'
 template='%(function)s(%(expressions)s, 2)'

def HomeListView(request):

    label = []
    dato = []

    puntaje = request.GET['puntaje']
    municipio = request.GET['municipio']
    inst = request.GET['inst']
    anios = request.GET['anios']

    if (anios == "TODOS"):
        if (municipio == "TODOS"):
            results = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(prom=Round(Avg(puntaje))).order_by('muni_id_ente__muni_nombre', 'anio')
            for entry in results:
                label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry['anio'])  # guarda nombre del departamento ya agrupado
                dato.append(entry['prom'])  # guarda promedio de cada grupo creado(por municipio)
        else:
            if (inst == "General"):
                results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                    prom=Round(Avg(puntaje))).filter(muni_id_ente__muni_nombre=municipio).order_by(
                    'id_institucion__ins_nombre', 'anio')
                for entry in results:
                    label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                        'anio'])  # guarda nombre del departamento ya agrupado
                    dato.append(entry['prom'])  # guarda promedio de cada grupo creado(por municipio)
            else:
                results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                    prom=Round(Avg(puntaje))).filter(muni_id_ente__muni_nombre=municipio,id_institucion__ins_nombre=inst).order_by(
                    'id_institucion__ins_nombre', 'anio')
                for entry in results:
                    label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                        'anio'])  # guarda nombre del departamento ya agrupado
                    dato.append(entry['prom'])  # guarda promedio de cada grupo creado(por municipio)
    else:
        if (municipio == "TODOS"):
            results = FactSaber5.objects.values('muni_id_ente__muni_nombre', 'anio').annotate(
                prom=Round(Avg(puntaje))).filter(anio=anios).order_by('muni_id_ente__muni_nombre', 'anio')
            for entry in results:
                label.append(entry['muni_id_ente__muni_nombre'] + " : " + entry[
                    'anio'])  # guarda nombre del departamento ya agrupado
                dato.append(entry['prom'])  # guarda promedio de cada grupo creado(por municipio)
        else:
            if (inst == "General"):
                results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                    prom=Round(Avg(puntaje))).filter(muni_id_ente__muni_nombre=municipio, anio=anios).order_by(
                    'id_institucion__ins_nombre', 'anio')
                for entry in results:
                    label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                        'anio'])  # guarda nombre del departamento ya agrupado
                    dato.append(entry['prom'])  # guarda promedio de cada grupo creado(por municipio)
            else:
                results = FactSaber5.objects.values('id_institucion__ins_nombre', 'anio').annotate(
                    prom=Round(Avg(puntaje))).filter(muni_id_ente__muni_nombre=municipio,
                                                     id_institucion__ins_nombre=inst, anio=anios).order_by(
                    'id_institucion__ins_nombre', 'anio')
                for entry in results:
                    label.append(entry['id_institucion__ins_nombre'] + " : " + entry[
                        'anio'])  # guarda nombre del departamento ya agrupado
                    dato.append(entry['prom'])  # guarda promedio de cada grupo creado(por municipio)


    return JsonResponse(data={

        'labels': label,
        'data': dato,
    })

def HomeHtml(request):
    title = "Consulta Dashboard >> Puntajes"
    list_url = reverse_lazy('appdash:home_list1')
    entity = 'Consulta'

    contexto = {"title": title, "list_url": list_url, "entity": entity}

    return render(request, "home/list.html", contexto)