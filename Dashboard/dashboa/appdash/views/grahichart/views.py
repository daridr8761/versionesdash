from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from django.db.models import Count, Avg, Sum
from appdash.forms import EstudianteForm
#from Dashboard.dashboa.appdash.forms import EstudianteForm


def graficahi(request):
    data = []
    munidata = []
    try:
        for i in FactSaber5.objects.values('muni_id_ente__muni_nombre').annotate(conta=Count('id_estudiante')):
            data.append(i['conta'])
            munidata.append(i['muni_id_ente__muni_nombre'])
    except Exception as e:
        data['error'] = str(e)
    return JsonResponse(data={
        'ynum': data,
        'xmuni': munidata,
    })


def grafichtmlhi(request):
    return render(request, "grahichart/list.html")




