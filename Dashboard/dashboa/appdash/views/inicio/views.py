from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5, DimPrueba, DimTiempo
from django.db.models import Sum, Count, Q, Avg
from django.db.models import Func
import pandas as pd
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import plotly.express as px
import numpy as np
from IPython.display import display
from appdash.forms import EstudianteForm
import psycopg2


class Round(Func):
    function = 'Round'
    arity = 2


def tabla_inicio(request):
    title = "Bienvenidos Analisis de Resultados Pruebas Saber 5"
    title1 = "Estudiantes"
    title2 = "Municipios "
    title3 = "Instituciones"
    title4 = "Asignaturas"
    num_pruebas = 4

    list_url = reverse_lazy('appdash:inicio_list')
    entity = 'Consulta'
    from django.db import connection
    with connection.cursor() as cursor:
        cursor.execute(
            "SELECT anio, CASE WHEN cantidad = 0 THEN 0 ELSE ROUND(CAST(suma as numeric(15,2)) / cantidad,2) END as Promedio FROM (SELECT anio, SUM(COALESCE(puntaje_lenguaje,0) + COALESCE(puntaje_matematicas,0) + COALESCE(puntaje_ciencias,0) + COALESCE(puntaje_competencias,0)) as suma, sum(CASE WHEN puntaje_lenguaje <> 0 THEN 1 ELSE 0 END + CASE WHEN puntaje_matematicas <> 0 THEN 1 ELSE 0 END + CASE WHEN puntaje_ciencias <> 0 THEN 1 ELSE 0 END + CASE WHEN puntaje_competencias <> 0 THEN 1 ELSE 0 END) cantidad FROM fact_saber5 GROUP BY 1,1 ORDER BY 1,1)T;")
        results = cursor.fetchall()

    with connection.cursor() as cursor:
        cursor.execute("SELECT COUNT(id_estudiante) as suma FROM fact_saber5")
        results1 = cursor.fetchall()

    with connection.cursor() as cursor:
        cursor.execute("SELECT COUNT(muni_id_ente) as suma FROM dim_municipios")
        results2 = cursor.fetchall()

    with connection.cursor() as cursor:
        cursor.execute("SELECT COUNT(id_institucion) as suma FROM dim_instituciones")
        results3 = cursor.fetchall()

        contexto = {
            "consultas": results,
            "consultas1": results1,
            "consultas2": results2,
            "consultas3": results3,
            "num_pruebas": num_pruebas,
            "title": title,
            "title1": title1,
            "title2": title2,
            "title3": title3,
            "title4": title4,
            "list_url": list_url,
            "entity": entity
        }
    return render(request, 'inicio/list.html', contexto)


def grafica_inicio(request):
    Aniosdata = []

    PuntLengEstu = []
    PuntMatEstu = []
    PuntCienEstu = []
    PuntCompEstu = []
    TipEsta = []
    Consulta = []

    try:
        for i in FactSaber5.objects.values('anio').annotate(
                p_lenguaje=Round(Avg('puntaje_lenguaje'), 2),
                p_matematicas=Round(Avg('puntaje_matematicas'), 2),
                p_ciencias=Round(Avg('puntaje_ciencias'), 2),
                p_competencias=Round(Avg('puntaje_competencias'), 2)
        ).order_by('anio'):
            Aniosdata.append(i['anio'])
            PuntLengEstu.append(i['p_lenguaje'])
            PuntMatEstu.append(i['p_matematicas'])
            PuntCienEstu.append(i['p_ciencias'])
            PuntCompEstu.append(i['p_competencias'])

    except Exception as e:
        Aniosdata['error'] = str(e)
        PuntLengEstu['error'] = str(e)
        PuntMatEstu['error'] = str(e)
        PuntCienEstu['error'] = str(e)
        PuntCompEstu['error'] = str(e)


    return JsonResponse(data={
        'xAnios': Aniosdata,
        'yPuntLengEstu': PuntLengEstu,
        'yPuntMatEstu': PuntMatEstu,
        'yPuntCienEstu': PuntCienEstu,
        'yPuntCompEstu': PuntCompEstu,
    })



# Descomentando de aqui en adelante y la linea donde llamamos en la plantilla a la grafica que se guarda en plt.savefig("appdash/static/inicio/js/Plot generated using Matplotlib.png") podemos
#visualizar la grafica que genera la BD Ecaes

''' datos = pd.read_csv("appdash/views/biplot_archivo/Ecaes.csv", sep="|", encoding="utf-8")
continuas = ['MOD_RAZONA_CUANTITAT_PUNT', 'MOD_LECTURA_CRITICA_PUNT', 'MOD_COMPETEN_CIUDADA_PUNT',
             'MOD_INGLES_PUNT',
             'PUNT_GLOBAL']


data = [datos[datos['ESTU_GENERO']=='F'].PUNT_GLOBAL, datos[datos['ESTU_GENERO']=='M'].PUNT_GLOBAL]
fig7, ax7 = plt.subplots()
ax7.set_title('boxplot comparativo PUNT_GLOBAL')
ax7.boxplot(data)
plt.savefig("appdash/static/inicio/js/Plot generated using Matplotlib.png")
#plt.show()  '''


