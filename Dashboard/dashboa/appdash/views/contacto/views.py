from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from django.db.models import Sum, Count, Q, Avg
from django.db.models import Func
from appdash.forms import EstudianteForm
import psycopg2


def contacto(request):
    title = "Grupo de Investigador"
    title1 = "Herramienta de Inteligencia de Negocios"
    integrante1 = "GRIAS"
    integrante2 = "UDENAR"
    celularInt1 = "Grupo de Investigación Aplicado en Sistemas"
    celularInt2 = "Universidad de Nariño"
    correoInt1 = "	grias@udenar.edu.co"
    correoInt2 = "www.udenar.edu.co"
    director = "Saber Expo BI 5"
    correodir = "grias@udenar.edu.co"

    list_url = reverse_lazy('appdash:contacto_list')
    entity = 'Consulta'

    contexto = {
                    "title": title,
                    "title1": title1,
                    "integrante1": integrante1,
                    "integrante2": integrante2,
                    "celularInt1": celularInt1,
                    "celularInt2": celularInt2,
                    "correoInt1": correoInt1,
                    "correoInt2": correoInt2,
                    "director": director,
                    "correodir": correodir,
                    "list_url": list_url,
                    "entity": entity
                }
    return render(request,'contacto/list.html', contexto)
