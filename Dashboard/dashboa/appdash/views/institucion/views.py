from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios
from appdash.forms import EstudianteForm


class InstitucionesListView(ListView):
    model = DimInstituciones
    template_name = 'institucion/list.html'

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        data = {}
        try:
            action = request.POST['action']
            if action == 'searchdata':
                data = []
                for i in DimInstituciones.objects.all():
                    data.append(i.toJSON())
            else:
                data['error'] = 'Ha ocurrido un error'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data, safe=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Instituciones'
        context['create_url'] = reverse_lazy('appdash:estudiante_create')
        context['list_url'] = reverse_lazy('appdash:institucion_list')
        #la anterior variable create_url nos lleva a la ruta de la vista estudiante_create donde la llamemos
        context['entity'] = 'Instituciones'
        #la anterior variable entity nos muestra el nombre Estudiantes donde la llamemos
        return context