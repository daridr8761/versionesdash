from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from appdash.models import DimEstudiante, DimInstituciones, DimMunicipios, FactSaber5
from django.db.models import Sum, Count, Q
from appdash.forms import EstudianteForm
import psycopg2


def NumEstuPorInstitucionListView(request):
    title = "Consulta Numero de Estudiantes por Año de Presentacion"
    list_url = reverse_lazy('appdash:NumEstuPorInstitucionListView_list')
    entity = 'Consulta'
    from django.db import connection
    with connection.cursor() as cursor:
        cursor.execute("SELECT anio, COUNT(id_estudiante) as suma FROM fact_saber5 GROUP BY 1,1 ORDER BY 1,1")
        results = cursor.fetchall()

        #rawData = list(cursor.fetchall())
        #rawData = namedtuplefetchall(cursor)
        #print(rawData[0][1])
        #result = []
        #for r in rawData:
         #   result.append(list(r))
        contexto = {"consultas": results,"title": title, "list_url": list_url, "entity": entity}
    return render(request,'NumEstuPorInstitucion/list.html', contexto)

def Num_Estu_Por_Institucion(request):
    Aniosdata = []
    NumEstu = []
    TipEsta = []
    try:
        for i in FactSaber5.objects.values('anio').annotate(
            num=Count('id_estudiante')).order_by('anio'):
            Aniosdata.append(i['anio'])
            NumEstu.append(i['num'])
    except Exception as e:
        Aniosdata['error'] = str(e)
        NumEstu['error'] = str(e)
    return JsonResponse(data={
        'xAnios': Aniosdata,
        'yNumEstu': NumEstu,
    })