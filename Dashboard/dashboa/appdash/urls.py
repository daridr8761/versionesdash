from os import name

from django.urls import path
from .views.estudiante.views import *
from .views.institucion.views import *
from .views.municipio.views import *
from .views.prueba.views import *
from .views.tiempo.views import *
from .views.resultado.views import *
from .views.NumEstuPorMunicipio.views import *
from .views.dashboard.views import *
from .views.graficaest.views import *
from .views.puntaje_ciencias_naturales.views import *
from .views.puntaje_lenguaje.views import *
from .views.puntaje_matematicas.views import *
from .views.puntaje_compencias.views import  *
from .views.NumEstuPorInstituion.views import *
from .views.inicio.views import *
from .views.Boxplot.views import *
from .views.Biplot.views import *
from .views.contacto.views import *
from .views.home.views import *
from .views.BD.views import *
from .views.dashboard2.views import *
from .views.dashboardGeneral.views import *

#from .views.grafica_est.views import *

app_name = 'appdash'

urlpatterns = [
    path('estudiante/list/', EstudianteListView.as_view(), name='estudiante_list'),
    path('institucion/list/', InstitucionesListView.as_view(), name='institucion_list'),
    path('municipio/list/', MunicipiosListView.as_view(), name='municipio_list'),
    path('prueba/list/', PruebasListView.as_view(), name='prueba_list'),
    path('tiempo/list/', TiempoListView.as_view(), name='tiempo_list'),
    path('resultado/list/', ResultadosListView.as_view(), name='resultado_list'),
    path('NumEstuPorMunicipio/list/', NumEstuPorMunicipioListView, name='NumEstuPorMunicipio_list'),
    path('dashboard/', DashboardView.as_view(), name='dashboard_list'),
    path('graficaest/list', grafichtml, name='graficaest_list'),
    path('grafica_est/', grafica, name='grafica_est'),
    #path('NumEstuPorTipoEsta/', Num_Estu_Por_MunicipioHtml, name='NumEstuPorTipoEsta'),
    path('NEstuPorMunicipio/list', Num_Estu_Por_Municipio, name='Num_Estu_Por_Municipio'),
    path('home/', HomeListView, name='home_list'),
    path('home/list/', HomeHtml, name='home_list1'),
    path('bd/list/', BdListView, name='BD_list'),
    path('tabla_puntaje_nat/list', tabla_puntaje_nat, name='tabla_puntaje_nat_list'),
    path('grafica_puntaje_nat/', grafica_puntaje_nat, name='grafica_puntaje_nat'),
    path('tabla_puntaje_leng/list', tabla_puntaje_leng, name='tabla_puntaje_leng_list'),
    path('grafica_puntaje_leng/', grafica_puntaje_leng, name='grafica_puntaje_leng'),
    path('tabla_puntaje_mat/list', tabla_puntaje_mat, name='tabla_puntaje_mat_list'),
    path('grafica_puntaje_mat', grafica_puntaje_mat, name='grafica_puntaje_mat'),
    path('tabla_puntaje_comp/list', tabla_puntaje_comp, name='tabla_puntaje_comp_list'),
    path('grafica_puntaje_comp', grafica_puntaje_comp, name='grafica_puntaje_comp'),
    path('NumEstuPorInstitucionListView/list', NumEstuPorInstitucionListView, name='NumEstuPorInstitucionListView_list'),
    path('Num_Estu_Por_Institucion', Num_Estu_Por_Institucion, name='Num_Estu_Por_Institucion'),
    path('inicio/list', tabla_inicio, name='inicio_list'),
    path('inicio', grafica_inicio, name='inicio'),
    path('contacto/list', contacto, name='contacto_list'),
    path('dashboard2/', DashboardListView, name='dashboard_list'),
    path('dashboard2/list/', DashboardHtml, name='dashboard_list1'),
    path('dashboard2_ge/', DashboardListViewGeneral, name='dashboard_listG'),
    path('dashboard2_ge/list/', DashboardHtmlGeneral, name='dashboard_list1G'),
    path('boxplot/list', tabla_boxplot, name='boxplot_list'),
    path('boxplot', grafica_boxplot, name='boxplot'),
    path('biplot/list', tabla_biplot, name='biplot_list'),
    path('biplot', grafica_biplot, name='biplot'),
    #path('graficaesthi/list', grafichtmlhi, name='graficaest_listhi'),
    #path('grafica_esthi/', graficahi, name='grafica_esthi'),
    #path('estudiante/list2/', estudiante_list, name='estudiante_list2'),
]