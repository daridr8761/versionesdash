# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.forms import model_to_dict


class DimEstudiante(models.Model):
    id_estudiante = models.AutoField(primary_key=True,unique=True,verbose_name='Id')
    estu_consecutivo = models.CharField(max_length=20,verbose_name='Consecutivo')
    estu_grupo = models.CharField(max_length=5,verbose_name='Grupo')
    estu_n = models.CharField(max_length=5,verbose_name='N')
    estu_estrato = models.CharField(max_length=10,verbose_name='Estrato')
    estu_grado = models.CharField(max_length=3,verbose_name='Grado')
    estu_sexo = models.CharField(max_length=15,verbose_name='Sexo')
    estu_dissenso = models.CharField(max_length=1,verbose_name='Dissenso')

    def __str__(self):
        #return self.muni_nombre
        return 'el id es %s el consecutivo es %s el sexo es %s estun es %s' % (self.id_estudiante,self.estu_consecutivo,self.estu_sexo,self.estu_n)

    # el metodo model_to_dict nos permite retornar consultas con tablas con muchos registros
    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'Estudiante'
        verbose_name_plural = 'Estudiantes'


    class Meta:
        managed = False
        db_table = 'dim_estudiante'


class DimInstituciones(models.Model):
    id_institucion = models.AutoField(primary_key=True,unique=True, verbose_name='Id')
    ins_codigo_dane = models.CharField(max_length=15, verbose_name='CodigoDane')
    ins_municipio = models.CharField(max_length=7, verbose_name='Municipio')
    ins_id_ente = models.CharField(max_length=4, verbose_name='IdEnte')
    ins_nombre = models.CharField(max_length=150, verbose_name='Nombre')
    ins_zona = models.CharField(max_length=15, verbose_name='Zona')
    ins_sector = models.CharField(max_length=15, verbose_name='Sector')
    ins_tipo_estab = models.CharField(max_length=30, verbose_name='TipoEsta')
    ins_calendario = models.CharField(max_length=2, verbose_name='Calendario')
    ins_nivel_socio = models.CharField(max_length=3, verbose_name='NivelSocioeconomico')

    # el metodo model_to_dict nos permite retornar consultas con tablas con muchos registros
    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'institucion'
        verbose_name_plural = 'Instituciones'

    class Meta:
        managed = False
        db_table = 'dim_instituciones'


class DimMunicipios(models.Model):
    muni_id_ente = models.IntegerField(primary_key=True,unique=True, verbose_name='Id_ente')
    muni_nombre = models.CharField(max_length=50, verbose_name='Nombre')
    muni_munexclu = models.CharField(max_length=15, verbose_name='MunExclu')
    muni_depar = models.CharField(max_length=20, verbose_name='Departamento')

    def __str__(self):
        return self.muni_nombre
        #return 'el nombre es %s el id es %s el municipio excluido es %s y el departamento es %s' % (self.muni_nombre,self.muni_id_ente,self.muni_munexclu,self.muni_depar)

    # el metodo model_to_dict nos permite retornar consultas con tablas con muchos registros
    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'municipio'
        verbose_name_plural = 'municipios'

    class Meta:
        managed = False
        db_table = 'dim_municipios'


class DimPrueba(models.Model):
    id_prueba = models.AutoField(primary_key=True,unique=True, verbose_name='Id')
    prue_estu_consecutivo = models.CharField(max_length=20, verbose_name='Estu_Consecutivo')
    prue_sn_lenguaje = models.CharField(max_length=20, blank=True, null=True, verbose_name='Lenguaje')
    prue_sn_matematicas = models.CharField(max_length=20, blank=True, null=True, verbose_name='Matematicas')
    prue_sn_ciencias_naturales = models.CharField(max_length=20, blank=True, null=True, verbose_name='C_Naturales')
    prue_sn_competencias = models.CharField(max_length=20, blank=True, null=True, verbose_name='Competencias')
    des_lenguaje = models.CharField(max_length=20, blank=True, null=True, verbose_name='D_lenguaje')
    des_matematicas = models.CharField(max_length=20, blank=True, null=True, verbose_name='D_matematicas')
    des_ciencias_naturales = models.CharField(max_length=20, blank=True, null=True, verbose_name='D_ciencias_naturales')
    des_competencias = models.CharField(max_length=20, blank=True, null=True, verbose_name='D_competencias')

    #el siguiente metodo str nos permite mostrar en fila cada uno de los atributos que coloquemos ahi cuando realicemos una consulta ORM de django
    def __str__(self):
        return 'el id es %s el consecutivo es %s el desempeño leguaje es %s desempeño mate es %s' % (self.id_prueba,self.prue_estu_consecutivo,self.des_lenguaje,self.des_matematicas)

    # el metodo model_to_dict nos permite retornar consultas con tablas con muchos registros
    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'prueba'
        verbose_name_plural = 'pruebas'

    class Meta:
        managed = False
        db_table = 'dim_prueba'


class DimTiempo(models.Model):
    id_tiempo = models.AutoField(primary_key=True,unique=True,verbose_name='Id_ti')
    prueba = models.CharField(max_length=50, verbose_name='Pru')
    anio = models.CharField(max_length=15, verbose_name='anio')

    def __str__(self):
        return 'el id es %s la prueba es %s y el año es %s' % (self.id_tiempo,self.prueba,self.anio)

    # el metodo model_to_dict nos permite retornar consultas con tablas con muchos registros
    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'tiempo'
        verbose_name_plural = 'tiempos'

    class Meta:
        managed = False
        db_table = 'dim_tiempo'


class FactSaber5(models.Model):
    id_estudiante = models.OneToOneField(DimEstudiante, models.DO_NOTHING, db_column='id_estudiante', primary_key=True, verbose_name='Id_Estu')
    muni_id_ente = models.ForeignKey(DimMunicipios, models.DO_NOTHING, db_column='muni_id_ente', verbose_name='MuniIdEnt')
    id_institucion = models.ForeignKey(DimInstituciones, models.DO_NOTHING, db_column='id_institucion', verbose_name='Id_Ins')
    id_prueba = models.ForeignKey(DimPrueba, models.DO_NOTHING, db_column='id_prueba', verbose_name='Id_Pru')
    id_tiempo = models.ForeignKey(DimTiempo, models.DO_NOTHING, db_column='id_tiempo', verbose_name='Id_Tiem')
    id_hoja = models.TextField(verbose_name='Id_Hoja')
    numero_estu = models.IntegerField(verbose_name='NumEstu')
    puntaje_lenguaje = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True, verbose_name='P_Lenguaje')
    puntaje_matematicas = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True, verbose_name='P_Mate')
    puntaje_ciencias = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True, verbose_name='P_Ciencias')
    puntaje_competencias = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True, verbose_name='P_Competencias')
    numero_lenguaje = models.IntegerField(blank=True, null=True, verbose_name='NumLegua')
    numero_matematicas = models.IntegerField(blank=True, null=True, verbose_name='NumMate')
    numero_ciencias = models.IntegerField(blank=True, null=True, verbose_name='NumCiencias')
    numero_competencias = models.IntegerField(blank=True, null=True, verbose_name='NumCompetencias')
    anio = models.TextField(blank=True, null=True, verbose_name='Anio')
    promedio = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True, verbose_name='prom')
    desempenio = models.CharField(max_length=1, verbose_name='des')

    # el metodo model_to_dict nos permite retornar consultas con tablas con muchos registros
    def toJSON(self):
        item = model_to_dict(self)
        return item

    class Meta:
        verbose_name = 'saber5'
        verbose_name_plural = 'sabers5'

    class Meta:
        managed = False
        db_table = 'fact_saber5'
        unique_together = (('id_estudiante', 'muni_id_ente', 'id_institucion', 'id_prueba'),)




class ArchivoCarga(models.Model):
    datos = models.FileField(upload_to='appdash/views/icfes/',null=True,blank=True)
